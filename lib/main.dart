// ignore_for_file: avoid_print, unused_import

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:pi_flutterapp/pages/about_page.dart';
import 'package:pi_flutterapp/pages/form_grant_csv.dart';
import 'package:pi_flutterapp/pages/insulator_opti.dart';
import 'package:pi_flutterapp/src/db/data_base.dart';
import 'package:pi_flutterapp/src/resources/app_colors.dart';

import 'package:sqflite_common_ffi/sqflite_ffi.dart';

Future main() async {
  if (Platform.isWindows || Platform.isLinux) {
    // Initialize FFI
    sqfliteFfiInit();
  }
  // Change the default factory. On iOS/Android, if not using `sqlite_flutter_lib` you can forget
  // this step, it will use the sqlite version available on the system.
  databaseFactory = databaseFactoryFfi;

  final db = DataBase.getInstance();
  await db
      .database; // Attendre 1 seconde pour que la base de données soit prête

  //runApp(const MyApp());
  // runApp(const MaterialApp(home: FormGrantCsv()));
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/home': (context) => const InsulatorOptiWidget(),
        '/form_grant_csv': (context) => const FormGrantCsv(),
        '/about': (context) => const AboutPage(),
      },
      home: const InsulatorOptiWidget(),
      theme: ThemeData(
        textTheme: const TextTheme().apply(
          bodyColor: AppColors.mainTextColor1,
          displayColor: AppColors.mainTextColor1,
        ),
      )));
}
