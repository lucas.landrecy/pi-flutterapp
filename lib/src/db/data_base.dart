// ignore: file_names
// ignore_for_file: avoid_print, prefer_for_elements_to_map_fromiterable, unused_local_variable

import 'dart:core';
import 'dart:io';

// For getting the directory to store the file
import 'package:pi_flutterapp/src/db/csv_reader.dart';
import 'package:pi_flutterapp/src/db/json_reader.dart';
import 'package:sqflite/sqflite.dart';

import 'data_class.dart';

class DataBase {
  final String _cantonTableCreation = """
      CREATE TABLE IF NOT EXISTS Canton(
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL UNIQUE
      );
  """;

  final String _grantTableCreation = """
      CREATE TABLE IF NOT EXISTS Grant(
        id INTEGER PRIMARY KEY,
        idCanton INTEGER NOT NULL,
        isLower BOOLEAN NOT NULL,
        U REAL NOT NULL,
        amountChfBy REAL NOT NULL,
        FOREIGN KEY (idCanton)
        REFERENCES Canton (id) 
      );
  """;

  // TypeMaterial
  final String _typeConstructionMaterialTableCreation = """
      CREATE TABLE IF NOT EXISTS TypeConstructionMaterial(
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL UNIQUE
      );
    """;

  final String _constructMaterialTalbeCreation = """
     CREATE TABLE IF NOT EXISTS ConstructMaterials (
        id INTEGER PRIMARY KEY,
        idType INTEGER NOT NULL,
        name LONGTEXT NOT NULL UNIQUE,
        density REAL NOT NULL,
        gwpConstruction REAL NOT NULL,
        gwpDebarras REAL NOT NULL,
        nreConstruction REAL NOT NULL,
        nreDebarras REAL NOT NULL,
        u REAL NOT NULL,
        price REAL NOT NULL,
        lifeTime INTEGER NOT NULL,
        FOREIGN KEY (idType)
        REFERENCES TypeConstructionMaterial (id) 
      );
    """;

  //Isolant
  final String _isolantTableCreation = """
    CREATE TABLE IF NOT EXISTS Isolant(
      id INTEGER PRIMARY KEY,
      material TEXT NOT NULL UNIQUE,
      masseVolumique REAL NOT NULL,
      gwpConstruction REAL NOT NULL,
      gwpDebarras REAL NOT NULL,
      nreConstruction REAL NOT NULL,
      nreDebarras REAL NOT NULL,
      u REAL NOT NULL,
      price10 REAL NOT NULL,
      price15 REAL NOT NULL,
      price20 REAL NOT NULL,
      price30 REAL NOT NULL,
      price40 REAL NOT NULL,
      price50 REAL NOT NULL,
      price60 REAL NOT NULL,
      price80 REAL NOT NULL,
      price100 REAL NOT NULL,
      price120 REAL NOT NULL,
      price140 REAL NOT NULL,
      price150 REAL NOT NULL,
      price160 REAL NOT NULL,
      price180 REAL NOT NULL,
      price200 REAL NOT NULL,
      price250 REAL NOT NULL,
      price300 REAL NOT NULL,
      price500 REAL NOT NULL
    );
    """;

  final String _typeHeatingTableCreation = """
      CREATE TABLE IF NOT EXISTS TypeHeatings (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL UNIQUE
      );
    """;

  final String _heatingTableCreation = """
      CREATE TABLE IF NOT EXISTS Heatings (
        id INTEGER PRIMARY KEY,
        idType INTEGER  NOT NULL,
        name TEXT NOT NULL UNIQUE,
        price REAL NOT NULL,
        gwp REAL NOT NULL,
        nre REAL NOT NULL,
        FOREIGN KEY (idType)
        REFERENCES TypeHeatings (id)
      );
    """;

  static const _databaseName = "EnerBat.db";
  static const _databaseVersion = 2;

  // make this a singleton class
  DataBase._privateConstructor();
  static DataBase? _instance;
  static DataBase getInstance() {
    _instance ??= DataBase._privateConstructor();
    return DataBase._instance!;
  }

  // only have a single app-wide reference to the database
  Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    await _initDatabase();
    return _database!;
  }

  // this opens the database (and creates it if it doesn't exist)
  Future<void> _initDatabase() async {
    const i = 0;
    String path = "${Directory.current.path}/$_databaseName";
    // String path = join("./", _databaseName);
    _database = await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate, singleInstance: true);
  }

  Future<void> _onCreate(Database db, int version) async {
    await _createFillCantonGrant(db);
    await _createFillMateriaux(db);
    await _createFillHeating(db);
    await _createFillIsolant(db);
  }

  Future<void> _createFillCantonGrant(Database db) async {
    await db.execute(_cantonTableCreation);
    await db.execute(_grantTableCreation);
    final cantons = await CsvReader.getListCanton();
    final Batch batch = db.batch();

    for (var element in cantons) {
      batch.insert("Canton", {"name": element.name.replaceAll(" ", "")});
    }
    final List<dynamic> result = await batch.commit();
    final int affectedRows = result.reduce((sum, element) => sum + 1);

    final grants = await CsvReader.getListGrants(null);
    for (var element in grants) {
      await insertGrant(element);
    }

    print(affectedRows);
  }

  Future<void> _createFillMateriaux(Database db) async {
    await db.execute(_typeConstructionMaterialTableCreation);
    await db.execute(_constructMaterialTalbeCreation);

    insertMaterialByCsv(null);
  }

  Future<void> _createFillHeating(Database db) async {
    await db.execute(_typeHeatingTableCreation);
    await db.execute(_heatingTableCreation);

    insertHeatingsByCsv(null);
  }

  Future<void> _createFillIsolant(Database db) async {
    await db.execute(_isolantTableCreation);

    insertIsolantByCsv(null);
  }

  // ---------------------- CANTON ----------------------
  Future<List<Canton>> getCantons() async {
    const String table = "Canton";
    const List<String> columns = ["*"];
    final Database db = await database;
    final List<Map<String, dynamic>> cantonMaps = await db.query(table,
        columns: columns); // Corrected line to execute the query

    return List.generate(cantonMaps.length, (i) {
      return Canton(
        id: cantonMaps[i]['id'],
        name: cantonMaps[i]['name'],
      );
    });
  }

  Future<Canton> getCanton(String cantonName) async {
    const String table = "Canton";
    const List<String> columns = ["*"];
    final Database db = await database;
    final List<Map<String, dynamic>> cantonMaps = await db.query(table,
        columns: columns,
        where: "name = ?",
        whereArgs: [cantonName]); // Corrected line to execute the query
    if (cantonMaps.isEmpty) return const Canton(id: 25, name: "Genève");
    return Canton(
      id: cantonMaps[0]['id'],
      name: cantonMaps[0]['name'],
    );
  }

  // ---------------------- GRANT ----------------------
  Future<void> insertGrant(Grant grant) async {
    final Database db = await database;
    String sql =
        "INSERT INTO Grant(idCanton, isLower, U, amountChfBy)SELECT id, ${grant.isLower ? 1 : 0}, ${grant.U}, ${grant.amountChfBy} FROM Canton WHERE name = \"${grant.nameCanton}\"";
    await db.rawInsert(sql);
  }

  Future<void> updateGrant(Grant grant) async {
    final Database db = await database;
    await db
        .update("Grant", grant.map(), where: "id = ?", whereArgs: [grant.id]);
  }

  Future<void> deleteGrant(Grant grant) async {
    final Database db = await database;
    await db.delete("Grant", where: "id = ?", whereArgs: [grant.id]);
  }

  Future<List<Grant>> getGrants(String cantonName) async {
    const List<String> columns = [
      "Grant.id as id",
      "Canton.name as CantonName",
      "isLower",
      "U",
      "amountChfBy"
    ];
    String sql =
        "SELECT ${columns.join(", ")} FROM Grant JOIN Canton ON Grant.idCanton = Canton.id WHERE Canton.name = ?";
    const String table = "Grant";

    Database db = await database;

    //
    final List<Map<String, dynamic>> grants =
        await db.rawQuery(sql, [cantonName]);

    return List.generate(grants.length, (i) {
      return Grant(
        id: grants[i]['id'],
        nameCanton: grants[i]['CantonName'],
        isLower: grants[i]['isLower'] == 1,
        U: grants[i]['U'],
        amountChfBy: grants[i]['amountChfBy'],
      );
    });
  }

  // ---------------------- Material ----------------------
  Future<Map<String, ConstructMaterial>> getMaterials() async {
    const String table = "ConstructMaterials";
    const List<String> columns = [
      "ConstructMaterials.*",
      "TypeConstructionMaterial.name as nameType"
    ];
    String sql =
        "SELECT ${columns.join(", ")} FROM $table JOIN TypeConstructionMaterial ON ConstructMaterials.idType = TypeConstructionMaterial.id";
    Database db = await database;
    final List<Map<String, dynamic>> materials = await db.rawQuery(sql);

    return Map.fromIterable(materials,
        key: (e) => e["name"],
        value: (e) => ConstructMaterial(
              id: e["id"],
              nameType: e["nameType"].toString(),
              name: e["name"],
              price: e["price"],
              u: e["u"],
              gwpConstruction: e["gwpConstruction"] ?? 0.0,
              gwpDebarras: e["gwpDebarras"] ?? 0.0,
              nreConstruction: e["nreConstruction"] ?? 0.0,
              nreDebarras: e["nreDebarras"] ?? 0.0,
              lifeTime: e["lifeTime"] ?? 30,
              density: e["density"] ?? 0.0,
            ));
  }

  Future<Map<String, ConstructMaterial>> getMaterialsByType(
      List<String> types) async {
    String typesString = types.join("\", \"");
    const String table = "ConstructMaterials";
    const List<String> columns = [
      "ConstructMaterials.*",
      "TypeConstructionMaterial.name as nameType"
    ];
    String sql =
        "SELECT ${columns.join(", ")} FROM $table JOIN TypeConstructionMaterial ON ConstructMaterials.idType = TypeConstructionMaterial.id WHERE TypeConstructionMaterial.name in (\"$typesString\")";
    Database db = await database;

    // SELECT * FROM ConstructMaterials JOIN TypeConstructionMaterial ON ConstructMaterials.idType = TypeConstructionMaterial.id WHERE TypeConstructionMaterial.name in ('Isolant', 'Mur')
    final List<Map<String, dynamic>> materials = await db.rawQuery(sql);

    return Map.fromIterable(materials,
        key: (e) => e["name"],
        value: (e) => ConstructMaterial(
              id: e["id"],
              nameType: e["nameType"].toString(),
              name: e["name"],
              price: e["price"],
              u: e["u"],
              gwpConstruction: e["gwpConstruction"] ?? 0.0,
              gwpDebarras: e["gwpDebarras"] ?? 0.0,
              nreConstruction: e["nreConstruction"] ?? 0.0,
              nreDebarras: e["nreDebarras"] ?? 0.0,
              lifeTime: e["lifeTime"] ?? 30,
              density: e["density"] ?? 0.0,
            ));
  }

  Future<ConstructMaterial> getMaterialByName(String name) async {
    const String table = "ConstructMaterials";
    const List<String> columns = ["*"];
    Database db = await database;
    final List<Map<String, dynamic>> materials = await db
        .query(table, columns: columns, where: "name = ?", whereArgs: [name]);

    return Map.fromIterable(materials,
        key: (e) => e["name"],
        value: (e) => ConstructMaterial(
              id: e["id"],
              nameType: e["nameType"].toString(),
              name: e["name"],
              price: e["price"],
              u: e["u"],
              gwpConstruction: e["gwpConstruction"] ?? 0.0,
              gwpDebarras: e["gwpDebarras"] ?? 0.0,
              nreConstruction: e["nreConstruction"] ?? 0.0,
              nreDebarras: e["nreDebarras"] ?? 0.0,
              lifeTime: e["lifeTime"] ?? 30,
              density: e["density"] ?? 0.0,
            )).values.first;
  }

  Future<List<ConstructMaterial>> getMaterialsByNames(
      List<String> names) async {
    const String table = "ConstructMaterials";
    const List<String> columns = ["*"];
    Database db = await database;

    names = names.map((e) => e.replaceAll("'", "\\'")).toList();

    final List<Map<String, dynamic>> materials = await db.query(table,
        columns: columns, where: "name in ('${names.join("', '")}')");

    return List.generate(materials.length, (i) {
      final e = materials[i];
      return ConstructMaterial(
        id: e["id"],
        nameType: e["nameType"].toString(),
        name: e["name"],
        price: e["price"],
        u: e["u"],
        gwpConstruction: e["gwpConstruction"] ?? 0.0,
        gwpDebarras: e["gwpDebarras"] ?? 0.0,
        nreConstruction: e["nreConstruction"] ?? 0.0,
        nreDebarras: e["nreDebarras"] ?? 0.0,
        lifeTime: e["lifeTime"] ?? 30,
        density: e["density"] ?? 0.0,
      );
    });
  }

  Future<void> insertMaterialByCsv(String? file) async {
    final Database db = await database;
    final List<ConstructMaterial> materials =
        await CsvReader.getListMaterial(file);
    final List<TypeConstructionMaterial> types =
        await CsvReader.getListTypeMaterial(file);

    Batch batch = db.batch();

    for (var element in types) {
      batch.insert("TypeConstructionMaterial", {"name": element.name});
    }
    await batch.commit();

    for (var element in materials) {
      try {
        final sql =
            """INSERT INTO ConstructMaterials(name, idType, density, gwpConstruction, gwpDebarras, nreConstruction, nreDebarras, u, price, lifeTime) 
          Select \"${element.name}\", TypeConstructionMaterial.id, ${element.density}, ${element.gwpConstruction}, ${element.gwpDebarras}, ${element.nreConstruction}, ${element.nreDebarras}, ${element.u}, ${element.price}, ${element.lifeTime}  
          From TypeConstructionMaterial 
          WHERE TypeConstructionMaterial.name =\"${element.nameType}\"
        """;

        await db.execute(sql);
      } catch (e) {
        if (e is DatabaseException && !e.isUniqueConstraintError()) {
          print("DatabaseExecption: " + e.toString());
        } else if (e is DatabaseException && e.isUniqueConstraintError()) {
          print("Existe deja: ${element.name}");
        } else {
          print("Error: " + e.toString());
        }
      }
    }
  }

  Future<void> delAllMaterial() async {
    final Database db = await database;
    await db.delete("ConstructMaterials");
  }

  // ---------------------- TypeMaterial ----------------------
  Future<List<TypeConstructionMaterial>> getTypeMaterial() async {
    const String table = "TypeConstructionMaterial";
    const List<String> columns = ["*"];
    final Database db = await database;
    final List<Map<String, dynamic>> materials =
        await db.query(table, columns: columns);

    return List.generate(
        materials.length,
        (i) => TypeConstructionMaterial(
            id: materials[i]["id"], name: materials[i]["name"]));
  }

  Future<void> delAllTypeMaterial() async {
    final Database db = await database;
    await db.delete("TypeConstructionMaterial");
  }

  // ---------------------- ISOLANT ----------------------
  Future<Map<String, Isolant>> getIsolants() async {
    const String table = "Isolant";
    String sql = "SELECT * FROM $table";
    Database db = await database;
    final List<Map<String, dynamic>> isolants = await db.rawQuery(sql);

    return Map.fromIterable(isolants,
        key: (e) => e["material"],
        value: (e) => Isolant(
              id: e["id"],
              material: e["material"],
              masseVolumique: e["masseVolumique"],
              gwpConstruction: e["gwpConstruction"],
              gwpDebarras: e["gwpDebarras"],
              nreConstruction: e["nreConstruction"],
              nreDebarras: e["nreDebarras"],
              u: e["u"],
              price10: e["price10"],
              price15: e["price15"],
              price20: e["price20"],
              price30: e["price30"],
              price40: e["price40"],
              price50: e["price50"],
              price60: e["price60"],
              price80: e["price80"],
              price100: e["price100"],
              price120: e["price120"],
              price140: e["price140"],
              price150: e["price150"],
              price160: e["price160"],
              price180: e["price180"],
              price200: e["price200"],
              price250: e["price250"],
              price300: e["price300"],
              price500: e["price500"],
            ));
  }

  Future<Isolant> getIsolantByName(String name) async {
    const String table = "Isolant";
    const List<String> columns = ["*"];
    Database db = await database;
    final List<Map<String, dynamic>> isolants = await db.query(table,
        columns: columns, where: "material = ?", whereArgs: [name]);

    return Map.fromIterable(isolants,
        key: (e) => e["name"],
        value: (e) => Isolant(
            id: e["id"],
            material: e["material"],
            masseVolumique: e["masseVolumique"],
            gwpConstruction: e["gwpConstruction"],
            gwpDebarras: e["gwpDebarras"],
            nreConstruction: e["nreConstruction"],
            nreDebarras: e["nreDebarras"],
            u: e["u"],
            price10: e["price10"],
            price15: e["price15"],
            price20: e["price20"],
            price30: e["price30"],
            price40: e["price40"],
            price50: e["price50"],
            price60: e["price60"],
            price80: e["price80"],
            price100: e["price100"],
            price120: e["price120"],
            price140: e["price140"],
            price150: e["price150"],
            price160: e["price160"],
            price180: e["price180"],
            price200: e["price200"],
            price250: e["price250"],
            price300: e["price300"],
            price500: e["price500"])).values.first;
  }

  Future getIsolantByNames(List<String> names) async {
    const String table = "Isolant";
    const List<String> columns = ["*"];
    Database db = await database;

    names = names.map((e) => e.replaceAll("'", "\\'")).toList();

    final List<Map<String, dynamic>> isolants = await db.query(table,
        columns: columns, where: "material in ('${names.join("', '")}')");

    return List.generate(isolants.length, (i) {
      final e = isolants[i];
      return Isolant(
        id: e["id"],
        material: e["material"],
        masseVolumique: e["masseVolumique"],
        gwpConstruction: e["gwpConstruction"],
        gwpDebarras: e["gwpDebarras"],
        nreConstruction: e["nreConstruction"],
        nreDebarras: e["nreDebarras"],
        u: e["u"],
        price10: e["price10"],
        price15: e["price15"],
        price20: e["price20"],
        price30: e["price30"],
        price40: e["price40"],
        price50: e["price50"],
        price60: e["price60"],
        price80: e["price80"],
        price100: e["price100"],
        price120: e["price120"],
        price140: e["price140"],
        price150: e["price150"],
        price160: e["price160"],
        price180: e["price180"],
        price200: e["price200"],
        price250: e["price250"],
        price300: e["price300"],
        price500: e["price500"],
      );
    });
  }

  Future<void> insertIsolantByCsv(String? file) async {
    final Database db = await database;
    final List<Isolant> isolants = await CsvReader.getListIsolant(file);

    Batch batch = db.batch();

    for (var element in isolants) {
      try {
        await db.execute(
            "INSERT INTO Isolant(material, masseVolumique, gwpConstruction, gwpDebarras, nreConstruction, nreDebarras, u, price10, price15, price20, price30, price40, price50, price60, price80, price100, price120, price140, price150, price160, price180, price200, price250, price300, price500) VALUES (\"${element.material}\", ${element.masseVolumique}, ${element.gwpConstruction}, ${element.gwpDebarras}, ${element.nreConstruction}, ${element.nreDebarras}, ${element.u}, ${element.price10}, ${element.price15}, ${element.price20}, ${element.price30}, ${element.price40}, ${element.price50}, ${element.price60}, ${element.price80}, ${element.price100}, ${element.price120}, ${element.price140}, ${element.price150}, ${element.price160}, ${element.price180}, ${element.price200}, ${element.price250}, ${element.price300}, ${element.price500})");
      } catch (e) {
        if (e is DatabaseException && !e.isUniqueConstraintError()) {
          print(e.toString());
        } else if (e is DatabaseException && e.isUniqueConstraintError()) {
          print("Existe deja: ${element.material}");
        }
      }
    }
  }

  Future<void> delAllIsolant() async {
    final Database db = await database;
    await db.delete("Isolant");
  }

  // ---------------------- HEATING ----------------------
  Future<Map<String, Heating>> getHeatings() async {
    const String table = "Heatings";
    const List<String> columns = ["*"];
    Database db = await database;
    final List<Map<String, dynamic>> heatings =
        await db.query(table, columns: columns);

    return Map.fromIterable(heatings,
        key: (e) => e["name"],
        value: (e) => Heating(
            id: e["id"],
            nameType: e["idType"].toString(),
            name: e["name"],
            price: e["price"],
            gwp: e["gwp"],
            nre: e["nre"]));
  }

  Future<Heating> getHeatingByName(String name) async {
    const String table = "Heatings";
    const List<String> columns = ["*"];
    Database db = await database;
    final List<Map<String, dynamic>> heatings = await db
        .query(table, columns: columns, where: "name = ?", whereArgs: [name]);

    return Map.fromIterable(heatings,
        key: (e) => e["name"],
        value: (e) => Heating(
            id: e["id"],
            nameType: e["idType"].toString(),
            name: e["name"],
            price: e["price"],
            gwp: e["gwp"],
            nre: e["nre"])).values.first;
  }

  Future<Map<String, Heating>> getHeatingsByType(List<String> types) async {
    const String table = "Heatings";
    const List<String> columns = ["*"];
    Database db = await database;

    // SELECT * FROM ConstructMaterials JOIN TypeConstructionMaterial ON ConstructMaterials.idType = TypeConstructionMaterial.id WHERE TypeConstructionMaterial.name in ('Isolant', 'Mur')
    final List<Map<String, dynamic>> heatings = await db.rawQuery(
        "SELECT * FROM Heatings JOIN TypeHeatings ON Heatings.idType = TypeHeatings.id WHERE TypeHeatings.name in (\"${types.join("\", \"")}");

    return Map.fromIterable(heatings,
        key: (e) => e["name"],
        value: (e) => Heating(
            id: e["id"],
            nameType: e["idType"].toString(),
            name: e["name"],
            price: e["price"],
            gwp: e["gwp"],
            nre: e["nre"]));
  }

  Future<void> insertHeatingsByCsv(String? file) async {
    final Database db = await database;
    List<TypeHeating> types = await CsvReader.getListTypeHeating(file);
    List<Heating> heatings = await CsvReader.getListHeating(file);

    Batch batch = db.batch();

    for (var element in types) {
      batch.insert("TypeHeatings", {"name": element.name});
    }
    await batch.commit();

    for (var element in heatings) {
      try {
        await db.execute(
            "INSERT INTO Heatings(name, idType, gwp, nre, price) Select \"${element.name}\", id, ${element.gwp}, ${element.nre}, ${element.price} From TypeHeatings where name =\"${element.nameType}\"");
      } catch (e) {
        if (e is DatabaseException && !e.isUniqueConstraintError()) {
          print(e.toString());
        } else if (e is DatabaseException && e.isUniqueConstraintError()) {
          print("Existe deja: ${element.name}");
        }
      }
    }
  }

  Future<void> delAllHeating() async {
    final Database db = await database;
    await db.delete("Heatings");
  }

  // ---------------------- TypeHeating ----------------------
  Future<void> delAllTypeHeating() async {
    final Database db = await database;
    await db.delete("TypeHeatings");
  }
  // ---------------------- CityTemp ----------------------

  static Future<List<CityTemp>> getCitytemp() async {
    return readJson("lib/src/csv/temperature.json");
  }

  static Future<CityTemp> getCityTempByName(String name) async {
    List<CityTemp> cityTemps = await getCitytemp();
    return cityTemps.firstWhere((element) => element.name == name);
  }
}
