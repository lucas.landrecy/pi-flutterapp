class Canton {
  final int id;
  final String name;

  const Canton({required this.id, required this.name});

  Map<String, dynamic> map() {
    return {"id": id, "name": name};
  }
}

class Grant {
  final int id;
  final String nameCanton;
  final bool isLower;
  final double U;
  final double amountChfBy;

  const Grant(
      {required this.id,
      required this.nameCanton,
      required this.isLower,
      required this.U,
      required this.amountChfBy});

  Map<String, dynamic> toMap() {
    return {
      "nameCanton": nameCanton,
      "isLower": isLower ? 1 : 0,
      "U": U,
      "amountChfBy": amountChfBy
    };
  }

  Map<String, dynamic> map() {
    return {
      "isLower": isLower ? 1 : 0,
      "U": U,
      "amountChfBy": amountChfBy
    };
  }
}

class TypeConstructionMaterial {
  final int id;
  final String name;

  const TypeConstructionMaterial({required this.id, required this.name});

  Map<String, dynamic> map() {
    return {"id": id, "name": name};
  }
}

class ConstructMaterial {
  final int id;
  final String name;
  final String nameType;
  final double price;
  final double u;
  final double density;
  final double gwpConstruction;
  final double gwpDebarras;
  final double nreConstruction;
  final double nreDebarras;

  final int lifeTime;

  // initallise all
  const ConstructMaterial({
    required this.id,
    required this.name,
    required this.nameType,
    required this.price,
    required this.u,
    required this.density,
    required this.gwpConstruction,
    required this.gwpDebarras,
    required this.nreConstruction,
    required this.nreDebarras,
    required this.lifeTime,
  });

  double get nre => nreConstruction + nreDebarras;
  double get gwp => gwpConstruction + gwpDebarras;

  Map<String, dynamic> map() {
    return {
      "name": name,
      "nameType": nameType,
      "price": price,
      "u": u,
      "density": density,
      "gwp_construction": gwpConstruction,
      "gwp_debarras": gwpDebarras,
      "lifeTime": lifeTime,
      "nre_construction": nreConstruction,
      "nre_debarras": nreDebarras
    };
  }

  @override
  String toString() {
    return map().toString();
  }
}

class Isolant {
  final int id;
  final String material;
  final double masseVolumique;
  final double gwpConstruction;
  final double gwpDebarras;
  final double nreConstruction;
  final double nreDebarras;
  final double u;
  final double price10;
  final double price15;
  final double price20;
  final double price30;
  final double price40;
  final double price50;
  final double price60;
  final double price80;
  final double price100;
  final double price120;
  final double price140;
  final double price150;
  final double price160;
  final double price180;
  final double price200;
  final double price250;
  final double price300;
  final double price500;

  Isolant({
    required this.id,
    required this.material,
    required this.masseVolumique,
    required this.gwpConstruction,
    required this.gwpDebarras,
    required this.nreConstruction,
    required this.nreDebarras,
    required this.u,
    required this.price10,
    required this.price15,
    required this.price20,
    required this.price30,
    required this.price40,
    required this.price50,
    required this.price60,
    required this.price80,
    required this.price100,
    required this.price120,
    required this.price140,
    required this.price150,
    required this.price160,
    required this.price180,
    required this.price200,
    required this.price250,
    required this.price300,
    required this.price500,
  });

  double get nre => nreConstruction + nreDebarras;
  double get gwp => gwpConstruction + gwpDebarras;
  double get densite => masseVolumique;
  double get density => masseVolumique;
  double get lifeTime => 30;

  Map<String, dynamic> map() {
    return {
      "material": material,
      "masseVolumique": masseVolumique,
      "gwp_construction": gwpConstruction,
      "gwp_debarras": gwpDebarras,
      "nre_construction": nreConstruction,
      "nre_debarras": nreDebarras,
      "u": u,
      "price10": price10,
      "price15": price15,
      "price20": price20,
      "price30": price30,
      "price40": price40,
      "price50": price50,
      "price60": price60,
      "price80": price80,
      "price100": price100,
      "price120": price120,
      "price140": price140,
      "price150": price150,
      "price160": price160,
      "price180": price180,
      "price200": price200,
      "price250": price250,
      "price300": price300,
      "price500": price500,
    };
  }

  @override
  String toString() {
    return map().toString();
  }
}

class TypeHeating {
  final int id;
  final String name;

  const TypeHeating({required this.id, required this.name});
}

class Heating {
  final int id;
  final String name;
  final String nameType;
  final double price;
  final double gwp;
  final double nre;

  // initallise all
  const Heating(
      {required this.id,
      required this.name,
      required this.nameType,
      required this.price,
      required this.gwp,
      required this.nre});
}

// Gestion des temp par zone

class MonthlyTemp {
  final String month;
  final double temp;

  MonthlyTemp(this.month, this.temp);

  // Fonction pour créer un objet MonthlyTemp à partir d'une map
  factory MonthlyTemp.fromMap(Map<String, dynamic> map) {
    var temp = map['temp'];
    if (temp is String) {
      temp = double.parse(temp);
    }
    if (temp is int) {
      temp = temp.toDouble();
    }
    return MonthlyTemp(map['month'], temp);
  }

  // Fonction pour convertir un objet MonthlyTemp en map
  Map<String, dynamic> toMap() {
    return {
      'month': month,
      'temp': temp,
    };
  }
}

class CityTemp {
  final String name;
  final List<MonthlyTemp> temps;
  final double yearTemp;

  CityTemp(this.name, this.temps, this.yearTemp);

  // Fonction pour créer un objet CityTemp à partir d'une map
  factory CityTemp.fromMap(Map<String, dynamic> map) {
    List<MonthlyTemp> temps = (map['temps'] as List)
        .map((item) => MonthlyTemp.fromMap(item))
        .toList();

    return CityTemp(map['name'], temps, map['Jahr']);
  }

  // Fonction pour convertir un objet CityTemp en map
  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'temps': temps.map((temp) => temp.toMap()).toList(),
      'Jahr': yearTemp,
    };
  }
}
