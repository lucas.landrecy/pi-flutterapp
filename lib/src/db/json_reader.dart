import 'dart:io';
import 'dart:convert';
import 'package:pi_flutterapp/src/db/data_class.dart';

Future<List<CityTemp>> readJson(String filePath) async {
  // Lire le fichier JSON
  var file = File("${Directory.current.path}/$filePath");
  var content = await file.readAsString();

  // Décoder le JSON en liste de maps
  var jsonData = jsonDecode(content) as List<dynamic>;

  // Convertir les maps en objets CityTemp
  List<CityTemp> cities =
      jsonData.map((cityMap) => CityTemp.fromMap(cityMap)).toList();

  return cities;
}
