// ignore_for_file: unused_local_variable, avoid_print

import 'dart:core';
import 'dart:io';

import 'package:csv/csv.dart';

import 'data_class.dart';

class CsvReader {
  static Future<String> _loadCsvFile(String path) async {
    final file = File('${Directory.current.path}/lib/$path');
    return file.readAsString();
  }

  static Future<List<Canton>> getListCanton() async {
    // Assuming _database is initialized and open
    final file = await _loadCsvFile('src/csv/canton.csv');
    final csvTable = const CsvToListConverter().convert(file,
        fieldDelimiter: ';',
        textDelimiter: "'",
        textEndDelimiter: "'",
        eol: "\n");

    return List.generate(
        csvTable.length, (index) => Canton(id: -1, name: csvTable[index][0]));
  }

  // Sub

  static Future<List<Grant>> getListGrants(String? file) async {
    if (file != null) {
      file = File(file).readAsStringSync();
    } else {
      file = await _loadCsvFile('src/csv/subventions.csv');
    }

    final csvTable = const CsvToListConverter().convert(file,
        fieldDelimiter: ',',
        textDelimiter: '"',
        textEndDelimiter: '"',
        eol: "\n",
        shouldParseNumbers: true);

    List<Grant> grants = List.empty(growable: true);

    for (var row in csvTable.skip(1)) {
      try {
        grants.add(Grant(
          id: int.parse(row[0]),
          nameCanton: row[1].replaceAll("'", " "),
          isLower: row[2].toLowerCase() == 'true',
          U: _mytoDouble(row[3]),
          amountChfBy: _mytoDouble(row[4]),
        ));
      } catch (e) {
        print("Erreur lors de la lecture de la ligne: $row");
      }
    }
    return grants;
  }

  static Future<List<TypeConstructionMaterial>> getListTypeMaterial(
      String? file) async {
    int name = 0;
    int masseVolumiqueSufface = 1;
    int unite = 2;
    int gwp = 3;
    int nre = 4;
    int price = 5;
    int u = 6;
    String nameType = "";

    // Assuming _database is initialized and open
    if (file != null) {
      file = File(file).readAsStringSync();
    } else {
      file = await _loadCsvFile('src/csv/materiaux.csv');
    }

    final csvTable = const CsvToListConverter().convert(file,
        fieldDelimiter: ';',
        textDelimiter: "\"",
        textEndDelimiter: "\"",
        eol: "\n",
        shouldParseNumbers: true);

    List<TypeConstructionMaterial> materials = List.empty(growable: true);

    for (var row in csvTable) {
      if (row[0] == "name") continue; // Saut de la première ligne
      if (row[0] != null && row[4] == "" && row[5] == "") {
        materials.add(TypeConstructionMaterial(
            id: -1, name: row[0].replaceAll("'", " ")));
      } else {
        continue;
      }
    }
    return materials;
  }

  static Future<List<ConstructMaterial>> getListMaterial(String? file) async {
    int name = 0;
    int masseVolumiqueSufface = 1;
    int gwpConstruction = 2;
    int gwpDebarras = 3;
    int nreConstruction = 4;
    int nreDebarras = 5;
    int u = 6;
    int price = 7;

    String nameType = "";

    // Assuming _database is initialized and open
    if (file != null) {
      file = File(file).readAsStringSync();
    } else {
      file = await _loadCsvFile('src/csv/materiaux.csv');
    }

    final csvTable = const CsvToListConverter().convert(file,
        fieldDelimiter: ';',
        textDelimiter: "\"",
        textEndDelimiter: "\"",
        eol: "\n",
        shouldParseNumbers: true);

    List<ConstructMaterial> materials = List.empty(growable: true);

    for (var row in csvTable) {
      if (row[name] == "name") continue; // Saut de la première ligne
      if (row[name] != null &&
          row[gwpConstruction] == "" &&
          row[gwpDebarras] == "") {
        nameType = row[0].replaceAll("'", " ");
      } else {
        if (row.length <= 7) continue;
        if (_mytoDouble(row[masseVolumiqueSufface]) == 0.0 ||
            (_mytoDouble(row[price]) == 0.0 && _mytoDouble(row[u]) == 0.0))
          continue;

        try {
          materials.add(ConstructMaterial(
              id: -1,
              name: row[name].replaceAll("'", " "),
              nameType: nameType,
              price: _mytoDouble(row[price]),
              u: _mytoDouble(row[u]),
              gwpConstruction: _mytoDouble(row[gwpConstruction]),
              gwpDebarras: _mytoDouble(row[gwpDebarras]),
              nreConstruction: _mytoDouble(row[nreConstruction]),
              nreDebarras: _mytoDouble(row[nreDebarras]),
              lifeTime: nameType == "Béton" ? 60 : 30,
              density: _mytoDouble(row[masseVolumiqueSufface])));
        } catch (e) {
          print("marche pas $row");
        }
      }
    }
    return materials;
  }

  static Future<List<Isolant>> getListIsolant(String? file) async {
    int name = 0;
    int masseVolumique = 1;
    int gwpConstruction = 2;
    int gwpDebarras = 3;
    int nreConstruction = 4;
    int nreDebarras = 5;
    int u = 6;
    int price10 = 7;
    int price15 = 8;
    int price20 = 9;
    int price30 = 10;
    int price40 = 11;
    int price50 = 12;
    int price60 = 13;
    int price80 = 14;
    int price100 = 15;
    int price120 = 16;
    int price140 = 17;
    int price150 = 18;
    int price160 = 19;
    int price180 = 20;
    int price200 = 21;
    int price250 = 22;
    int price300 = 23;
    int price500 = 24;

    String nameType = "";

    // Assuming _database is initialized and open
    if (file != null) {
      file = File(file).readAsStringSync();
    } else {
      file = await _loadCsvFile('src/csv/isolant.csv');
    }

    final csvTable = const CsvToListConverter().convert(file,
        fieldDelimiter: ';',
        textDelimiter: "\"",
        textEndDelimiter: "\"",
        eol: "\n",
        shouldParseNumbers: true);

    List<Isolant> isolants = List.empty(growable: true);

    for (var row in csvTable) {
      if (row[name] == "name") continue; // Saut de la première ligne
      if (row[name] != null &&
          row[gwpConstruction] == "" &&
          row[gwpDebarras] == "") {
        nameType = row[0].replaceAll("'", " ");
      } else {
        if (row.length <= 24) continue;
        if (_mytoDouble(row[masseVolumique]) == 0.0 ||
            (_mytoDouble(row[price10]) == 0.0 &&
                _mytoDouble(row[price15]) == 0.0 &&
                _mytoDouble(row[price20]) == 0.0 &&
                _mytoDouble(row[price30]) == 0.0 &&
                _mytoDouble(row[price40]) == 0.0 &&
                _mytoDouble(row[price50]) == 0.0 &&
                _mytoDouble(row[price60]) == 0.0 &&
                _mytoDouble(row[price80]) == 0.0 &&
                _mytoDouble(row[price100]) == 0.0 &&
                _mytoDouble(row[price120]) == 0.0 &&
                _mytoDouble(row[price140]) == 0.0 &&
                _mytoDouble(row[price150]) == 0.0 &&
                _mytoDouble(row[price160]) == 0.0 &&
                _mytoDouble(row[price180]) == 0.0 &&
                _mytoDouble(row[price200]) == 0.0 &&
                _mytoDouble(row[price250]) == 0.0 &&
                _mytoDouble(row[price300]) == 0.0 &&
                _mytoDouble(row[price500]) == 0.0))
          continue;

        isolants.add(Isolant(
          id: -1,
          material: row[name].replaceAll("'", " "),
          masseVolumique: _mytoDouble(row[masseVolumique]),
          gwpConstruction: _mytoDouble(row[gwpConstruction]),
          gwpDebarras: _mytoDouble(row[gwpDebarras]),
          nreConstruction: _mytoDouble(row[nreConstruction]),
          nreDebarras: _mytoDouble(row[nreDebarras]),
          u: _mytoDouble(row[u]),
          price10: _mytoDouble(row[price10]),
          price15: _mytoDouble(row[price15]),
          price20: _mytoDouble(row[price20]),
          price30: _mytoDouble(row[price30]),
          price40: _mytoDouble(row[price40]),
          price50: _mytoDouble(row[price50]),
          price60: _mytoDouble(row[price60]),
          price80: _mytoDouble(row[price80]),
          price100: _mytoDouble(row[price100]),
          price120: _mytoDouble(row[price120]),
          price140: _mytoDouble(row[price140]),
          price150: _mytoDouble(row[price150]),
          price160: _mytoDouble(row[price160]),
          price180: _mytoDouble(row[price180]),
          price200: _mytoDouble(row[price200]),
          price250: _mytoDouble(row[price250]),
          price300: _mytoDouble(row[price300]),
          price500: _mytoDouble(row[price500]),
        ));
      }
    }

    return isolants;
  }

  static Future<List<TypeHeating>> getListTypeHeating(String? file) async {
    int name = 0;
    int gwp = 1;
    int nre = 2;
    int price = 3;
    String nameType = "";

    // Assuming _database is initialized and open
    if (file != null) {
      file = File(file).readAsStringSync();
    } else {
      file = await _loadCsvFile('src/csv/chauffage.csv');
    }

    final csvTable = const CsvToListConverter().convert(file,
        fieldDelimiter: ';',
        textDelimiter: "\"",
        textEndDelimiter: "\"",
        eol: "\n");

    List<TypeHeating> typeHeating = List.empty(growable: true);

    for (var row in csvTable) {
      try {
        if (row[0] == "name") continue; // Saut de la première ligne
        if (row[0] != null && row[1] == "" && row[2] == "") {
          typeHeating
              .add(TypeHeating(id: -1, name: row[0].replaceAll("'", " ")));
        } else {
          continue;
        }
      } catch (e) {
        print("marche pas $row");
      }
    }

    return typeHeating;
  }

  static Future<List<Heating>> getListHeating(String? file) async {
    int name = 0;
    int gwp = 1;
    int nre = 2;
    int price = 3;
    String nameType = "";

    // Assuming _database is initialized and open
    if (file != null) {
      file = File(file).readAsStringSync();
    } else {
      file = await _loadCsvFile('src/csv/chauffage.csv');
    }

    final csvTable = const CsvToListConverter().convert(file,
        fieldDelimiter: ';',
        textDelimiter: "\"",
        textEndDelimiter: "\"",
        eol: "\n");

    List<Heating> typeHeating = List.empty(growable: true);

    for (var row in csvTable) {
      try {
        if (row[0] == "name" || row[0] == "") {
          continue; // Saut de la première ligne
        }

        if (row[0] != null && row[1] == "" && row[2] == "") {
          nameType = row[0].replaceAll("'", " ");
        } else {
          if (row.length <= 3) continue;
          if (_mytoDouble(row[price]) == 0.0) continue;

          typeHeating.add(Heating(
            id: -1,
            name: row[name].replaceAll("'", " "),
            nameType: nameType,
            price: _mytoDouble(row[price]),
            gwp: _mytoDouble(row[gwp]),
            nre: _mytoDouble(row[nre]),
          ));
        }
      } catch (e) {
        print("marche pas $row");
      }
    }

    return typeHeating;
  }

  static double _mytoDouble(dynamic e) {
    if (e == null) {
      return 0.0;
    }
    double value;
    try {
      if (e is String) {
        e = e.replaceAll(",", ".");
        e = e.replaceAll(" ", "");
        value = double.parse(e);
      } else {
        value = (e as num).toDouble();
      }
    } catch (error) {
      print("DoubleError: $e, $error");
      value = 0.0;
    }

    return value;
  }
}
