// ignore_for_file: avoid_print, non_constant_identifier_names

import 'dart:ffi';

import 'package:fl_chart/fl_chart.dart';
import 'package:pi_flutterapp/src/db/data_base.dart';
import 'package:pi_flutterapp/src/db/data_class.dart';

class Calculateur {
  // ----------------- ISOLANT -----------------
  /// <summary>
  /// Calcul de la perte thermique annuelle
  /// </summary>
  /// <param name="couches">Liste des couches de la paroi</param>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <param name="city">Températures mensuelles de la ville</param>
  /// <returns>La perte thermique annuelle</returns>
  /// <remarks>La perte thermique est exprimée en Kw</remarks>
  double perteAnnuelleThermique(
      List<Couche> couches, Isolant isolant, double epaisseur, CityTemp city) {
    double u = coefTransThermique(couches, isolant, epaisseur);

    List<double> pertesMensuelles = city.temps.map((temp) {
      double deltaT = 20 - temp.temp;
      double surface = 1.0;
      return perteThermiqueMensuelle(deltaT, surface, u, temp.month);
    }).toList();

    double result = perteThermiqueAnnuelle(pertesMensuelles) / 1000;
    return result;
  }

  double perteThermiqueMensuelle(
      double deltaT, double surface, double resistanceTotale, String month) {
    return deltaT * surface * resistanceTotale * H(month);
  }

  double perteThermiqueAnnuelle(List<double> pertesMensuelles) {
    return pertesMensuelles.reduce((somme, perte) => somme + perte);
  }

  int H(month) {
    if (["Apr", "Jun", "Sep", "Nov"].contains(month)) {
      return 24 * 30;
    } else if (["Jan", "Mar", "Mai", "Jul", "Aug", "Okt", "Dez"]
        .contains(month)) {
      return 24 * 31;
    } else {
      return 24 * 28;
    }
  }

  /// <summary>
  /// Calcul du coefficient de transmission thermique
  /// </summary>
  /// <param name="couches">Liste des couches de la paroi</param>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <returns>Le coefficient de transmission thermique</returns>
  double coefTransThermique(
      List<Couche> couches, Isolant isolant, double epaisseur) {
    double resistanceTotale = 0.0;
    for (var couche in couches) {
      resistanceTotale += couche.resistance;
    }
    resistanceTotale += epaisseur / isolant.u;
    resistanceTotale += 0.13; // Rsi
    resistanceTotale += 0.04; // Rse
    return 1 / resistanceTotale;
  }

  /// <summary>
  /// Calcul du GWP de l'isolant
  /// </summary>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <returns>Le GWP de l'isolant</returns>
  /// <remarks>Le GWP est exprimé en kg CO2/m²</remarks>
  double GWPIsolant(ConstructMaterial isolant, double epaisseur) {
    return isolant.gwp * isolant.density * epaisseur;
  }

  /// <summary>
  /// Calcul du NRE de l'isolant
  /// </summary>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <returns>Le NRE de l'isolant</returns>
  /// <remarks>Le NRE est exprimé en MJ/m²</remarks>
  double GWPConstruction(List<Couche> couches, Isolant isolant,
      double epaisseur, double houseLifeTime) {
    double GWPConstMat = 0.0;
    double GWPRemMat = 0.0;
    double GWPElimMat = 0.0;
    for (var couche in couches) {
      GWPConstMat += couche.ConstructionGWP;

      double nbOfRempl = ((houseLifeTime / (couche.isPorter ? 60 : 30)) - 1);
      double mremmat = couche.epaisseur * couche.density * nbOfRempl;
      double tempRempl = mremmat * couche.gwpConstruction;

      GWPRemMat += tempRempl;
      double tempElim =
          (mremmat + couche.epaisseur * couche.density) * couche.gwpDebarras;
      GWPElimMat += tempElim;
    }

    double tempisoconst = isolant.gwpConstruction * isolant.density * epaisseur;

    GWPConstMat += tempisoconst;
    double tempisorempl = (houseLifeTime / isolant.lifeTime - 1) *
        isolant.gwpConstruction *
        isolant.density *
        epaisseur;

    GWPRemMat += tempisorempl;
    double tempisoelim = (isolant.gwpDebarras * isolant.density * epaisseur) +
        (houseLifeTime / isolant.lifeTime - 1) *
            isolant.gwpDebarras *
            isolant.density *
            epaisseur;

    GWPElimMat += tempisoelim;
    return GWPConstMat + GWPRemMat + GWPElimMat;
  }

  /// <summary>
  /// Calcul du NRE de l'isolant
  /// </summary>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <returns>Le NRE de l'isolant</returns>
  /// <remarks>Le NRE est exprimé en MJ/m²</remarks>
  double NREIsolant(Isolant isolant, double epaisseur) {
    return isolant.nre * isolant.density * epaisseur;
  }

  /// <summary>
  /// Calcul du NRE de l'isolant
  /// </summary>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <returns>Le NRE de l'isolant</returns>
  /// <remarks>Le NRE est exprimé en MJ/m²</remarks>
  double NREConstruction(List<Couche> couches, Isolant isolant,
      double epaisseur, double houseLifeTime) {
    double NREConstMat = 0.0;
    double NRERemMat = 0.0;
    double NREElimMat = 0.0;
    for (var couche in couches) {
      NREConstMat += couche.ConstructionNRE;

      double nbOfRempl = ((houseLifeTime / (couche.isPorter ? 60 : 30)) - 1);
      double mremmat = couche.epaisseur * couche.density * nbOfRempl;
      double tempRempl = mremmat * couche.nreConstruction;

      NRERemMat += tempRempl;
      double tempElim =
          (mremmat + couche.epaisseur * couche.density) * couche.nreDebarras;
      NREElimMat += tempElim;
    }

    double tempisoconst = isolant.nreConstruction * isolant.density * epaisseur;

    NREConstMat += tempisoconst;
    double tempisorempl = (houseLifeTime / isolant.lifeTime - 1) *
        isolant.nreConstruction *
        isolant.density *
        epaisseur;

    NRERemMat += tempisorempl;
    double tempisoelim = (isolant.nreDebarras * isolant.density * epaisseur) +
        (houseLifeTime / isolant.lifeTime - 1) *
            isolant.nreDebarras *
            isolant.density *
            epaisseur;

    NREElimMat += tempisoelim;
    return NREConstMat + NRERemMat + NREElimMat;
  }

  /// <summary>
  /// Calcul du coût de l'isolant
  /// </summary>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <param name="grants">Liste des subventions</param>
  /// <returns>Le coût de l'isolant</returns>
  double CoutIsolant(Isolant isolant, double epaisseur, List<Grant> grants,
      {double surface = 1.0}) {
    double cout = 0;
    double tmpEpaisseur = epaisseur;
    final EpaisseurKown = [
      0.01,
      0.015,
      0.02,
      0.03,
      0.04,
      0.05,
      0.06,
      0.08,
      0.1,
      0.12,
      0.14,
      0.15,
      0.16,
      0.18,
      0.2,
      0.25,
      0.3,
      0.5
    ];
    while (tmpEpaisseur >= 0.01) {
      for (var epaisseurKown in EpaisseurKown.reversed) {
        if (tmpEpaisseur >= epaisseurKown) {
          switch (epaisseurKown) {
            case 0.01:
              cout += isolant.price10 * surface;
              break;
            case 0.015:
              cout += isolant.price15 * surface;
              break;
            case 0.02:
              cout += isolant.price20 * surface;
              break;
            case 0.03:
              cout += isolant.price30 * surface;
              break;
            case 0.04:
              cout += isolant.price40 * surface;
              break;
            case 0.05:
              cout += isolant.price50 * surface;
              break;
            case 0.06:
              cout += isolant.price60 * surface;
              break;
            case 0.08:
              cout += isolant.price80 * surface;
              break;
            case 0.1:
              cout += isolant.price100 * surface;
              break;
            case 0.12:
              cout += isolant.price120 * surface;
              break;
            case 0.14:
              cout += isolant.price140 * surface;
              break;
            case 0.15:
              cout += isolant.price150 * surface;
              break;
            case 0.16:
              cout += isolant.price160 * surface;
              break;
            case 0.18:
              cout += isolant.price180 * surface;
              break;
            case 0.2:
              cout += isolant.price200 * surface;
              break;
            case 0.25:
              cout += isolant.price250 * surface;
              break;
            case 0.3:
              cout += isolant.price300 * surface;
              break;
            case 0.5:
              cout += isolant.price500 * surface;
              break;
          }
          tmpEpaisseur -= epaisseurKown;
          break;
        }
      }
    }

    // Appliquer la subvention pour Genève uniquement
    if (grants.isNotEmpty) {
      double totalSubvention = 0.0;
      for (var grant in grants) {
        totalSubvention += grant.amountChfBy * surface;
      }
      cout -= totalSubvention;
    }

    return cout;
  }

  double CoutMur(List<Couche> couches, double epaisseur, List<Grant> grants,
      {double surface = 1.0, int houseLifeTime = 60}) {
    double cost = 0.0;
    for (var couche in couches) {
      cost += couche.price *
          couche.epaisseur *
          100 *
          surface *
          (houseLifeTime / (couche.isPorter ? 60 : 30));
    }
    return cost;
  }

  // ----------------- CHAUFFAGE -----------------

  /// <summary>
  /// Calcul du GWP de l'énergie
  /// </summary>
  /// <param name="couches">Liste des couches de la paroi</param>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <param name="city">Températures mensuelles de la ville</param>
  /// <param name="heating">Type de chauffage</param>
  /// <returns>Le GWP de l'énergie</returns>
  /// <remarks>Le GWP est exprimé en kg CO2</remarks>
  double GWPChauffage(List<Couche> couches, Isolant isolant, double epaisseur,
      CityTemp city, Heating heating, double houseLifeTime) {
    double isolantPerteTherm =
        perteAnnuelleThermique(couches, isolant, epaisseur, city) *
            houseLifeTime;
    return isolantPerteTherm * heating.gwp;
  }

  /// <summary>
  /// Calcul du NRE de l'énergie
  /// </summary>
  /// <param name="couches">Liste des couches de la paroi</param>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <param name="city">Températures mensuelles de la ville</param>
  /// <param name="heating">Type de chauffage</param>
  /// <returns>Le NRE de l'énergie</returns>
  /// <remarks>Le NRE est exprimé en MJ</remarks>
  double NREChauffage(List<Couche> couches, Isolant isolant, double epaisseur,
      CityTemp city, Heating heating, double houseLifeTime) {
    double isolantPerteTherm =
        perteAnnuelleThermique(couches, isolant, epaisseur, city) *
            houseLifeTime;
    return isolantPerteTherm * heating.nre;
  }

  /// <summary>
  /// Calcul du coût de l'énergie
  /// </summary>
  /// <param name="couches">Liste des couches de la paroi</param>
  /// <param name="isolant">Matériau isolant</param>
  /// <param name="epaisseur">Epaisseur de l'isolant</param>
  /// <param name="city">Températures mensuelles de la ville</param>
  /// <param name="heating">Type de chauffage</param>
  /// <returns>Le coût de l'énergie</returns>
  /// <remarks>Le coût est exprimé en €</remarks>
  double CoutChauffage(List<Couche> couches, Isolant isolant, double epaisseur,
      CityTemp city, Heating heating, double houseLifeTime) {
    double isolantPerteTherm =
        perteAnnuelleThermique(couches, isolant, epaisseur, city) *
            houseLifeTime;
    return isolantPerteTherm * heating.price;
  }
}

class Couche {
  final double epaisseur;
  final ConstructMaterial material;
  final bool isPorter;

  const Couche(
      {required this.epaisseur, required this.material, this.isPorter = false});

  double get conductiviteThermique => material.u;
  double get density => material.density;
  int get lifeTime => material.lifeTime;

  double get gwp => material.gwp * material.density * epaisseur;
  double get gwpConstruction => material.gwpConstruction;
  double get gwpDebarras => material.gwpDebarras;
  double get ConstructionGWP =>
      material.gwpConstruction * material.density * epaisseur;
  double get DebarrasGWP => material.gwpDebarras * material.density * epaisseur;

  double get nre => material.nre * material.density * epaisseur;
  double get nreConstruction => material.nreConstruction;
  double get nreDebarras => material.nreDebarras;
  double get ConstructionNRE =>
      material.nreConstruction * material.density * epaisseur;
  double get DebarrasNRE => material.nreDebarras * material.density * epaisseur;

  double get price => material.price * epaisseur;
  double get resistance => epaisseur / conductiviteThermique;
}

// ------------ Isolant ------------
List<FlSpot> getGWPIsolant(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double gwpValue =
        calculateur.GWPConstruction(couches, isolant, epaisseur, 60);
    points.add(FlSpot(epaisseur, gwpValue));
  }

  return points;
}

List<FlSpot> getNREIsolant(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double nreValue =
        calculateur.NREConstruction(couches, isolant, epaisseur, 60);
    points.add(FlSpot(epaisseur, nreValue));
  }

  return points;
}

List<FlSpot> getCostIsolant(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment,
    {double surface = 1.0}) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double costValue =
        calculateur.CoutIsolant(isolant, epaisseur, grants, surface: surface) +
            calculateur.CoutMur(couches, epaisseur, grants, surface: surface);
    points.add(FlSpot(epaisseur, costValue));
  }

  return points;
}

// ------------ Chauffage ------------
List<FlSpot> getGWPChauffage(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double gwpValue = calculateur.GWPChauffage(
        couches, isolant, epaisseur, cityTemp, heating, 60);
    points.add(FlSpot(epaisseur, gwpValue));
  }

  return points;
}

List<FlSpot> getNREChauffage(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double nreValue = calculateur.NREChauffage(
        couches, isolant, epaisseur, cityTemp, heating, 60);
    points.add(FlSpot(epaisseur, nreValue));
  }

  return points;
}

List<FlSpot> getCostChauffage(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment,
    {double surface = 1.0}) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double costValue = calculateur.CoutChauffage(
        couches, isolant, epaisseur, cityTemp, heating, 60);
    points.add(FlSpot(epaisseur, costValue));
  }

  return points;
}

// ------------ Total ------------
List<FlSpot> getGWP(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double gwpValue = calculateur.GWPChauffage(
            couches, isolant, epaisseur, cityTemp, heating, 60) +
        calculateur.GWPConstruction(couches, isolant, epaisseur, 60);
    points.add(FlSpot(epaisseur, gwpValue));
  }

  return points;
}

List<FlSpot> getNRE(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double nreValue = calculateur.NREChauffage(
            couches, isolant, epaisseur, cityTemp, heating, 60) +
        calculateur.NREConstruction(couches, isolant, epaisseur, 60);
    points.add(FlSpot(epaisseur, nreValue));
  }

  return points;
}

List<FlSpot> getCost(
    List<Couche> couches,
    Isolant isolant,
    Heating heating,
    List<Grant> grants,
    CityTemp cityTemp,
    double min,
    double max,
    double increment,
    {double surface = 1.0}) {
  List<FlSpot> points = [];
  Calculateur calculateur = Calculateur();

  for (double epaisseur = min; epaisseur <= max; epaisseur += increment) {
    double costValue = calculateur.CoutChauffage(
            couches, isolant, epaisseur, cityTemp, heating, 60) +
        calculateur.CoutMur(couches, epaisseur, grants, surface: surface) +
        calculateur.CoutIsolant(isolant, epaisseur, grants, surface: surface);
    points.add(FlSpot(epaisseur, costValue));
  }

  return points;
}

void main() async {
  Calculateur calculateur = Calculateur();

  const List<Couche> couches = [
    Couche(
        epaisseur: 0.015,
        material: ConstructMaterial(
            u: 0.7,
            gwpConstruction: 0.138,
            gwpDebarras: 0.013,
            nreConstruction: 0.65,
            nreDebarras: 0.65,
            price: 10,
            density: 1400,
            lifeTime: 30,
            id: 1,
            name: "Enduit mortier intérieur (minéral)",
            nameType: "test")),
    Couche(
        epaisseur: 0.175,
        material: ConstructMaterial(
            u: 0.35,
            gwpConstruction: 0.254,
            gwpDebarras: 0.013,
            nreConstruction: 0.65,
            nreDebarras: 0.65,
            price: 20,
            density: 1100,
            lifeTime: 60,
            id: 2,
            name: "Brique en terre cuite (BTC normal 25)",
            nameType: "test")),
    Couche(
        epaisseur: 0.003,
        material: ConstructMaterial(
            u: 0.87,
            gwpConstruction: 0.393,
            gwpDebarras: 0.013,
            nreConstruction: 0.65,
            nreDebarras: 0.65,
            price: 10,
            density: 1030,
            lifeTime: 30,
            id: 3,
            name: "Mortier combiné Iso KK70 / mortier d'enrobage minéral",
            nameType: "test")),
    Couche(
        epaisseur: 0.025,
        material: ConstructMaterial(
            u: 0.87,
            gwpConstruction: 0.138,
            gwpDebarras: 0.013,
            nreConstruction: 0.65,
            nreDebarras: 0.65,
            price: 10,
            density: 1800,
            lifeTime: 30,
            id: 5,
            name: "Enduit mortier extérieur (minéral)",
            nameType: "test")),
  ];

  // const ConstructMaterial isolant = ConstructMaterial(
  //     u: 0.038,
  //     gwpConstruction: 3.86,
  //     gwpDebarras: 3.09,
  //     nreConstruction: 0.78,
  //     nreDebarras: 0.62,
  //     price: 50,
  //     density: 15,
  //     lifeTime: 30,
  //     id: 2,
  //     name: "EPS",
  //     nameType: "isolant");

  // const Heating heating = Heating(
  //     id: 1,
  //     name: "PAC air eau",
  //     gwp: 0.04,
  //     nre: 0.78,
  //     price: 4,
  //     nameType: 'test');

  // const double houseLifeTime = 60;

  // CityTemp city = await DataBase.getCityTempByName("Bern Liebefeld");
  // print(
  //     "U: " + calculateur.coefTransThermique(couches, isolant, 0.3).toString());
  // double GWPenergie = calculateur.GWPChauffage(
  //     couches, isolant, 0.3, city, heating, houseLifeTime);
  // print("GWP chauffage: " + GWPenergie.toString());
  // double GWPCoutConstr =
  //     calculateur.GWPConstruction(couches, isolant, 0.3, houseLifeTime);
  // print("GWP construction: " + GWPCoutConstr.toString());
  // print("GWP total: " + (GWPenergie + GWPCoutConstr).toString());
}
