import 'package:equatable/equatable.dart';

class DropdownModel extends Equatable {
  final int id;
  final String value;

  const DropdownModel({required this.id, required this.value});

  @override
  List<Object?> get props => [id, value];
}
