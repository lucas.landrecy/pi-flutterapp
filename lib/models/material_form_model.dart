import 'package:equatable/equatable.dart';

class MaterialFormModel extends Equatable {
  static int counter = 0;

  final int index = counter++;
  final String materialSelected;
  final double width;
  final bool isPorteur;

  MaterialFormModel(
      {required this.materialSelected,
      required this.width,
      required this.isPorteur});

  @override
  List<Object> get props => [index, materialSelected, width, isPorteur];
}
