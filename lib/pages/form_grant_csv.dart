import 'package:flutter/material.dart';
import 'package:pi_flutterapp/src/resources/app_colors.dart';
import 'package:pi_flutterapp/widgets/csv_form/csv_form.dart';
import 'package:pi_flutterapp/widgets/grant_from/grant_form.dart';
import 'package:pi_flutterapp/widgets/nav.dart';

class FormGrantCsv extends StatelessWidget {
  const FormGrantCsv({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('Formulaire de subvention et Materiaux, Heating',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColors.mainTextColor1)),
            backgroundColor: AppColors.primary),
        body: const Column(
          children: [
            Nav(),
            Row(children: [
              Expanded(child: GrantFormWidget()),
              Expanded(child: CsvForm())
            ]),
          ],
        ));
  }
}
