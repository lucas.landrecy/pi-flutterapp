import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:pi_flutterapp/block/material_line/material_form_bloc.dart';
import 'package:pi_flutterapp/main.dart';
import 'package:pi_flutterapp/models/material_form_model.dart';
import 'package:pi_flutterapp/pages/about_page.dart';
import 'package:pi_flutterapp/src/calc/calc.dart';
import 'package:pi_flutterapp/src/db/data_base.dart';
import 'package:pi_flutterapp/src/db/data_class.dart';
import 'package:pi_flutterapp/src/resources/app_colors.dart';
import 'package:pi_flutterapp/widgets/chart/charts.dart';
import 'package:pi_flutterapp/widgets/nav.dart';
import 'package:pi_flutterapp/widgets/wall_form/wall_form_widget.dart';

class InsulatorOptiWidget extends StatefulWidget {
  const InsulatorOptiWidget({super.key});

  @override
  State<InsulatorOptiWidget> createState() => _InsulatorOpti();
}

class _InsulatorOpti extends State<InsulatorOptiWidget> {
  final double min = 0.01;
  final double max = 0.5;
  final double interval = 0.05;
  MaterialFormLoaded? _materialFormState;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.pageBackground,
      appBar: AppBar(
          title: const Text('Outil de calcul d\'isolation',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: AppColors.mainTextColor1)),
          backgroundColor: AppColors.primary),
      body: Column(children: [
        const Nav(),
        Row(children: [
          Expanded(
              child: WallFormWidget(
            onUpdated: updateChart,
            onSubmitted: SubmitChart,
          )),
          Expanded(
              child: _materialFormState != null
                  ? ChartWidget(
                      materialFormState: _materialFormState!,
                      minX: min,
                      maxX: max,
                      intervalX: interval)
                  : Container()),
        ]),
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const AboutPage()),
          );
        },
        child: const Icon(Icons.help_outline),
      ),
    );
  }

  void updateChart(List<MaterialFormModel>? materialFormModel, String? canton,
      String? heating, String? insulator, String? weather) {
    List<MaterialFormModel> materials = materialFormModel ??
        (_materialFormState != null
            ? _materialFormState!.materialFormModel
            : List.empty());
    String cantonSelected = (canton != null && canton != "")
        ? canton!
        : (_materialFormState != null
            ? _materialFormState!.cantonSelected
            : "");
    String heatingSelected = heating != null && heating != ""
        ? heating!
        : (_materialFormState != null
            ? _materialFormState!.heatingSelected
            : "");
    String insulatorSelected = insulator != null && insulator != ""
        ? insulator!
        : (_materialFormState != null
            ? _materialFormState!.insulatorSelected
            : "");
    String weatherSelected = weather != null && weather != ""
        ? weather!
        : (_materialFormState != null
            ? _materialFormState!.weatherSelected
            : "");

    final MaterialFormLoaded materialFormLoaded = MaterialFormLoaded(
        materialFormModel: materials,
        cantonSelected: cantonSelected,
        heatingSelected: heatingSelected,
        insulatorSelected: insulatorSelected,
        weatherSelected: weatherSelected);

    _materialFormState = materialFormLoaded;
  }

  void SubmitChart() {
    setState(() {
      _materialFormState = _materialFormState;
    });
  }
}
