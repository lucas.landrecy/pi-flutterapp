import 'package:flutter/material.dart';
import 'package:pi_flutterapp/block/material_line/material_form_bloc.dart';
import 'package:pi_flutterapp/src/calc/calc.dart';
import 'package:pi_flutterapp/src/db/data_base.dart';
import 'package:pi_flutterapp/src/db/data_class.dart';
import 'package:pi_flutterapp/src/resources/app_colors.dart';

import 'package:fl_chart/fl_chart.dart';

class ChartWidget extends StatefulWidget {
  final MaterialFormLoaded materialFormState;
  final double minX;
  final double maxX;
  final double intervalX;

  ChartWidget(
      {super.key,
      required this.materialFormState,
      required this.minX,
      required this.maxX,
      required this.intervalX});

  @override
  // ignore: no_logic_in_create_state
  State<ChartWidget> createState() => _ChartWidgetState(surface: 100.0);
}

class _MinValue {
  final double x;
  final double nre;
  final double gwp;
  final double cost;

  _MinValue(
      {required this.x,
      required this.nre,
      required this.gwp,
      required this.cost});
}

class _ChartWidgetState extends State<ChartWidget> {
  int chartType = 0;
  final List<List<String>> chartTypes = [
    ['GWP Chaffage', 'GWPMateriaux', 'GWP Total'],
    ['NRE Chaffage', 'NRE Materiaux', 'NRE Total'],
    ['Cout Chaffage', 'Cout Materiaux', 'Cout Total']
  ];

  final double surface;
  _ChartWidgetState({required this.surface});

  changeTypeChart() {
    print("changeTypeChart");
    setState(() {
      chartType = chartType == 2 ? 0 : chartType + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder<List<List<List<FlSpot>>>>(
            future: getSpots(),
            builder: (context, snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return const CircularProgressIndicator();
              }
              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              }
              final data = snapshot.data!;
              final minValue = getMinValue(data[0][2], data[1][2], data[2][2]);

              final minValueGWP =
                  getMinValueGWP(data[0][2], data[1][2], data[2][2]);

              final minValueNRE =
                  getMinValueNRE(data[0][2], data[1][2], data[2][2]);

              final minValueCost =
                  getMinValueCost(data[0][2], data[1][2], data[2][2]);

              return Column(children: [
                Row(children: [
                  Text(chartTypes[chartType][0],
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: AppColors.contentColorBlue)),
                  const SizedBox(width: 20),
                  Text(chartTypes[chartType][1],
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: AppColors.contentColorGreen)),
                  const SizedBox(width: 20),
                  Text(chartTypes[chartType][2],
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: AppColors.contentColorOrange)),
                  const SizedBox(width: 20),
                  ElevatedButton(
                      onPressed: () => changeTypeChart(),
                      child: Text(chartType == 0
                          ? 'GWP -> NRE'
                          : chartType == 1
                              ? 'NRE -> Cout'
                              : 'Cout -> GWP')),
                ]),
                const SizedBox(height: 20),
                Stack(children: <Widget>[
                  AspectRatio(
                    aspectRatio: 1.70,
                    child: Padding(
                        padding: const EdgeInsets.only(
                          right: 18,
                          left: 20,
                          top: 24,
                          bottom: 12,
                        ),
                        child: LineChartSample2(
                          allSpots: data[chartType],
                          minX: widget.minX,
                          maxX: widget.maxX,
                          intervalX: widget.intervalX,
                        )),
                  ),
                ]),
                const SizedBox(height: 20),
                Flex(
                    direction: Axis.horizontal,
                    clipBehavior: Clip.antiAlias,
                    children: [
                      Text("""Valeur minimale 
de l'ensemble des courbes:
      - X: ${minValue.x.toStringAsFixed(2)}
      - NRE: ${minValue.nre.toStringAsFixed(2)}
      - GWP: ${minValue.gwp.toStringAsFixed(2)}
      - Cout: ${minValue.cost.toStringAsFixed(2)}
                    """),
                      const SizedBox(width: 20),
                      Text("""Valeur minimale GWP:
      - X: ${minValueGWP.x.toStringAsFixed(2)}
      - NRE total: ${minValueGWP.nre.toStringAsFixed(2)}
      - Cout total: ${minValueGWP.cost.toStringAsFixed(2)}
      - GWP Total: ${minValueGWP.gwp.toStringAsFixed(2)}
      - GWP isolant: ${data[0][1].where((element) => element.x == minValueGWP.x).first.y.toStringAsFixed(2)}
      - GWP chauffage: ${data[0][0].where((element) => element.x == minValueGWP.x).first.y.toStringAsFixed(2)}
                    """),
                      const SizedBox(width: 20),
                      Text("""Valeur minimale NRE:
      - X: ${minValueNRE.x.toStringAsFixed(2)}
      - GWP total: ${minValueNRE.gwp.toStringAsFixed(2)}
      - Cout total: ${minValueNRE.cost.toStringAsFixed(2)}
      - NRE total: ${minValueNRE.nre.toStringAsFixed(2)}
      - NRE isolant: ${data[1][1].where((element) => element.x == minValueNRE.x).first.y.toStringAsFixed(2)}
      - NRE chauffage: ${data[1][0].where((element) => element.x == minValueNRE.x).first.y.toStringAsFixed(2)}
                    """),
                      const SizedBox(width: 20),
                      Text("""Valeur minimale Cout:
      - X: ${minValueCost.x.toStringAsFixed(2)}
      - NRE: ${minValueCost.nre.toStringAsFixed(2)}
      - GWP: ${minValueCost.gwp.toStringAsFixed(2)}
      - Cout total: ${minValueCost.cost.toStringAsFixed(2)}
      - Cout isolant: ${data[2][1].where((element) => element.x == minValueCost.x).first.y.toStringAsFixed(2)}
      - Cout chauffage: ${data[2][0].where((element) => element.x == minValueCost.x).first.y.toStringAsFixed(2)}
                    """),
                    ])
              ]);
            }));
  }

  Future<List<List<List<FlSpot>>>> getSpots() async {
    MaterialFormLoaded materialFormState = widget.materialFormState;
    if (materialFormState == null) {
      return const [];
    }
    widget.materialFormState;
    if (materialFormState.materialFormModel.isEmpty ||
        materialFormState.materialFormModel[0].materialSelected == "" ||
        materialFormState.insulatorSelected == "" ||
        materialFormState.heatingSelected == "" ||
        materialFormState.cantonSelected == "" ||
        materialFormState.weatherSelected == "") {
      return const [];
    }

    DataBase db = DataBase.getInstance();
    List<String> materialNames = List.generate(
        materialFormState!.materialFormModel.length,
        (index) =>
            materialFormState!.materialFormModel[index].materialSelected);
    materialNames = materialNames.toSet().toList();
    materialNames.remove("");
    List<ConstructMaterial> materials =
        await db.getMaterialsByNames(materialNames);

    List<Couche> couches = List.generate(
        materialNames.length,
        (index) => Couche(
            material: materials[index],
            epaisseur: materialFormState!.materialFormModel
                    .firstWhere((element) =>
                        element.materialSelected == materials[index].name)
                    .width *
                0.01,
            isPorter: materialFormState!.materialFormModel
                .firstWhere((element) =>
                    element.materialSelected == materials[index].name)
                .isPorteur));

    Isolant insulator =
        await db.getIsolantByName(materialFormState!.insulatorSelected);

    Heating heating =
        await db.getHeatingByName(materialFormState!.heatingSelected);
    List<Grant> grants = await db.getGrants(materialFormState!.cantonSelected);
    CityTemp cityTemp =
        await DataBase.getCityTempByName(materialFormState!.weatherSelected);

    return [
      gwpSpots(couches, insulator, heating, grants, cityTemp, widget.minX,
          widget.maxX, widget.intervalX),
      nreSpots(couches, insulator, heating, grants, cityTemp, widget.minX,
          widget.maxX, widget.intervalX),
      costSpots(couches, insulator, heating, grants, cityTemp, widget.minX,
          widget.maxX, widget.intervalX)
    ];
  }

  List<List<FlSpot>> gwpSpots(
      List<Couche> couches,
      Isolant insulator,
      Heating heating,
      List<Grant> grants,
      CityTemp cityTemp,
      double min,
      double max,
      double increment) {
    return [
      getGWPChauffage(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
      getGWPIsolant(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
      getGWP(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
    ];
  }

  List<List<FlSpot>> nreSpots(
      List<Couche> couches,
      Isolant insulator,
      Heating heating,
      List<Grant> grants,
      CityTemp cityTemp,
      double min,
      double max,
      double increment) {
    return [
      getNREChauffage(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
      getNREIsolant(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
      getNRE(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
    ];
  }

  List<List<FlSpot>> costSpots(
      List<Couche> couches,
      Isolant insulator,
      Heating heating,
      List<Grant> grants,
      CityTemp cityTemp,
      double min,
      double max,
      double increment) {
    return [
      getCostChauffage(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
      getCostIsolant(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
      getCost(couches, insulator, heating, grants, cityTemp, min, max,
          increment / 2),
    ];
  }

  _MinValue getMinValue(
      List<FlSpot> listNre, List<FlSpot> listGwp, List<FlSpot> listCost) {
    double nre = double.infinity;
    double gwp = double.infinity;
    double cost = double.infinity;
    double minX = double.infinity;

    for (int i = 0; i < listNre.length; i++) {
      if (listNre[i].y < nre && listGwp[i].y < gwp && listCost[i].y < cost) {
        minX = listNre[i].x;
        nre = listNre[i].y;
        gwp = listGwp[i].y;
        cost = listCost[i].y;
      } else if (listNre[i].y > nre &&
          listGwp[i].y < gwp &&
          listCost[i].y < cost) {
        final deltaNre = listNre[i].y - nre;
        final deltaGwp = gwp - listGwp[i].y;
        final deltaCost = cost - listCost[i].y;
        if (deltaNre < (deltaGwp + deltaCost)) {
          minX = listNre[i].x;
          nre = listNre[i].y;
          gwp = listGwp[i].y;
          cost = listCost[i].y;
        }
      } else if (listNre[i].y < nre &&
          listGwp[i].y > gwp &&
          listCost[i].y < cost) {
        final deltaNre = nre - listNre[i].y;
        final deltaGwp = listGwp[i].y - gwp;
        final deltaCost = cost - listCost[i].y;
        if (deltaGwp < deltaNre + deltaCost) {
          minX = listNre[i].x;
          nre = listNre[i].y;
          gwp = listGwp[i].y;
          cost = listCost[i].y;
        }
      } else if (listNre[i].y < nre &&
          listGwp[i].y < gwp &&
          listCost[i].y > cost) {
        final deltaNre = nre - listNre[i].y;
        final deltaGwp = gwp - listGwp[i].y;
        final deltaCost = listCost[i].y - cost;
        if (deltaCost < deltaNre + deltaGwp) {
          minX = listNre[i].x;
          nre = listNre[i].y;
          gwp = listGwp[i].y;
          cost = listCost[i].y;
        }
      }
    }

    return _MinValue(x: minX, nre: nre, gwp: gwp, cost: cost);
  }

  _MinValue getMinValueGWP(
      List<FlSpot> listGwp, List<FlSpot> listNre, List<FlSpot> listCost) {
    double nre = double.infinity;
    double gwp = double.infinity;
    double cost = double.infinity;
    double minX = double.infinity;

    for (int i = 0; i < listGwp.length; i++) {
      if (listGwp[i].y < gwp) {
        minX = listGwp[i].x;
        nre = listNre[i].y;
        gwp = listGwp[i].y;
        cost = listCost[i].y;
      }
    }
    return _MinValue(x: minX, nre: nre, gwp: gwp, cost: cost);
  }

  _MinValue getMinValueNRE(
      List<FlSpot> listGwp, List<FlSpot> listNre, List<FlSpot> listCost) {
    double nre = double.infinity;
    double gwp = double.infinity;
    double cost = double.infinity;
    double minX = double.infinity;

    for (int i = 0; i < listNre.length; i++) {
      if (listNre[i].y < nre) {
        minX = listGwp[i].x;
        nre = listNre[i].y;
        gwp = listGwp[i].y;
        cost = listCost[i].y;
      }
    }
    return _MinValue(x: minX, nre: nre, gwp: gwp, cost: cost);
  }

  _MinValue getMinValueCost(
      List<FlSpot> listGwp, List<FlSpot> listNre, List<FlSpot> listCost) {
    double nre = double.infinity;
    double gwp = double.infinity;
    double cost = double.infinity;
    double minX = double.infinity;

    for (int i = 0; i < listCost.length; i++) {
      if (listCost[i].y < cost) {
        minX = listGwp[i].x;
        nre = listNre[i].y;
        gwp = listGwp[i].y;
        cost = listCost[i].y;
      }
    }
    return _MinValue(x: minX, nre: nre, gwp: gwp, cost: cost);
  }
}

class LineChartSample2 extends StatelessWidget {
  final List<LinearGradient> gradientColors = const [
    LinearGradient(
      colors: [
        AppColors.contentColorBlue,
        AppColors.contentColorBlue,
      ],
    ),
    LinearGradient(
      colors: [
        AppColors.contentColorGreen,
        AppColors.contentColorGreen,
      ],
    ),
    LinearGradient(
      colors: [
        AppColors.contentColorOrange,
        AppColors.contentColorOrange,
      ],
    ),
    LinearGradient(colors: [
      AppColors.contentColorRed,
      AppColors.contentColorRed,
    ])
  ];

  final List<List<FlSpot>> allSpots;
  final double minX;
  final double maxX;
  final double intervalX;

  final double minY = 0;
  late double maxY;
  double get intervalY => maxY / 10;

  LineChartSample2(
      {super.key,
      required this.allSpots,
      required this.minX,
      required this.maxX,
      required this.intervalX}) {
    if (allSpots.length != 3)
      maxY = 500;
    else {
      final un = allSpots.first.first.y;
      final deux = allSpots[1].first.y;
      final trois = allSpots.last.first.y;
      maxY = un > deux ? un : deux;
      maxY = maxY > trois ? maxY : trois;
    }
  }

  @override
  Widget build(BuildContext context) {
    return LineChart(
      mainData(),
    );
  }

  /// Represents the state of the LineChartSample2 widget.
  ///
  /// This class is responsible for managing the state of the LineChartSample2 widget,
  /// including the data to be displayed, the appearance of the chart, and the interaction
  /// with the user.
  ///
  /// The LineChartSample2 widget displays a line chart with customizable data points and
  /// grid lines. The chart can be toggled between showing the average data and the main data.
  ///
  /// The LineChartSample2State class extends the [State] class and overrides the [build]
  /// method to build the widget tree. It also defines helper methods for generating the
  /// titles and widgets for the chart axes.
  LineChartData mainData() => LineChartData(
        lineTouchData: const LineTouchData(enabled: false),
        gridData: gridData(),
        titlesData: titlesData(),
        borderData: FlBorderData(
          show: true,
          border: Border.all(color: AppColors.contentColorBlack, width: 1),
        ),
        minX: minX,
        maxX: maxX,
        minY: minY,
        maxY: maxY,
        lineBarsData: linesBarData(),
      );

  FlGridData gridData() => FlGridData(
        show: true,
        drawHorizontalLine: true,
        horizontalInterval: intervalY,
        verticalInterval: intervalX,
        getDrawingHorizontalLine: (value) {
          return const FlLine(
            color: AppColors.mainGridLineColor,
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return const FlLine(
            color: AppColors.mainGridLineColor,
            strokeWidth: 1,
          );
        },
      );

  FlTitlesData titlesData() => FlTitlesData(
        show: true,
        rightTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            reservedSize: 42,
            interval: intervalX, // interval between each title
            getTitlesWidget: bottomTitleWidgets,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            interval: intervalY, // interval between each title
            getTitlesWidget: leftTitleWidgets,
            reservedSize: 60,
          ),
        ),
      );

  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
    Widget text = Text(value.toStringAsFixed(2), style: style);

    return SideTitleWidget(
      axisSide: meta.axisSide,
      child: text,
    );
  }

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );
    String text = value.toStringAsFixed(0);

    return Text(text, style: style, textAlign: TextAlign.left);
  }

  List<LineChartBarData> linesBarData() {
    if (allSpots.isEmpty) {
      return List.empty(growable: true);
    }

    List<LineChartBarData> linesBarData = List.empty(growable: true);

    for (int i = 0; i < allSpots.length; i++) {
      linesBarData.add(
        LineChartBarData(
          spots: allSpots[i],
          isCurved: true,
          gradient: gradientColors[i],
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: const FlDotData(show: false),
          belowBarData: BarAreaData(
            show: false,
          ),
        ),
      );
    }

    return linesBarData;
  }
}
