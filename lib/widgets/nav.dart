import 'package:flutter/material.dart';

class Nav extends StatelessWidget {
  const Nav({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: TextButton(
                onPressed: () {
                  Navigator.popAndPushNamed(context, '/home');
                },
                child: const Text(
                  'Optimisation d\'isolation',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )),
          const SizedBox(width: 20),
          Expanded(
              flex: 2,
              child: TextButton(
                onPressed: () {
                  Navigator.popAndPushNamed(context, '/form_grant_csv');
                },
                child: const Text(
                  'Formulaire de subvention',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )),
          const SizedBox(width: 20),
          Expanded(
              flex: 2,
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/about');
                },
                child: const Text(
                  'A propos',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )),
        ],
      ),
    );
  }
}
