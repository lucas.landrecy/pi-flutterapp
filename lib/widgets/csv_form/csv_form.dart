// ignore_for_file: avoid_print

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:pi_flutterapp/src/db/data_base.dart';

class CsvForm extends StatelessWidget {
  const CsvForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
            'Selectionner les fichiers csv contenant les nouvelles données des matériaux'),
        ElevatedButton(
          onPressed: () async {
            final String rootPath = Platform.isWindows ? 'C:\\' : '/home';
            final file = await FilesystemPicker.open(
              title: 'Parcourir les fichiers',
              context: context,
              rootDirectory: Directory(rootPath),
              fsType: FilesystemType.file,
              pickText: 'Save file',
              folderIconColor: Colors.teal,
              allowedExtensions: ['.csv'],
            );

            DataBase db = DataBase.getInstance();
            db.delAllMaterial();
            db.delAllTypeMaterial();

            db.insertMaterialByCsv(file);
          },
          child: const Text('Parcourir les fichiers'),
        ),
        const SizedBox(height: 20),
        const Text(
            'Selectionner les fichiers csv contenant les nouvelles données des chauffages'),
        ElevatedButton(
          onPressed: () async {
            final String rootPath = Platform.isWindows ? 'C:\\' : '/home';
            final file = await FilesystemPicker.open(
              title: 'Parcourir les fichiers',
              context: context,
              rootDirectory: Directory(rootPath),
              fsType: FilesystemType.file,
              pickText: 'Save file',
              folderIconColor: Colors.teal,
              allowedExtensions: ['.csv'],
            );

            DataBase db = DataBase.getInstance();
            db.delAllHeating();
            db.delAllTypeHeating();

            db.insertHeatingsByCsv(file);
          },
          child: const Text('Parcourir les fichiers'),
        ),
        const SizedBox(height: 20),
        const Text(
            'Selectionner les fichiers csv contenant les nouvelles données des isolants'),
        ElevatedButton(
          onPressed: () async {
            final String rootPath = Platform.isWindows ? 'C:\\' : '/home';
            final file = await FilesystemPicker.open(
              title: 'Parcourir les fichiers',
              context: context,
              rootDirectory: Directory(rootPath),
              fsType: FilesystemType.file,
              pickText: 'Save file',
              folderIconColor: Colors.teal,
              allowedExtensions: ['.csv'],
            );

            DataBase db = DataBase.getInstance();
            db.delAllIsolant();

            db.insertIsolantByCsv(file);
          },
          child: const Text('Parcourir les fichiers'),
        ),
      ],
    );
  }
}
