// ignore_for_file: overridden_fields

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pi_flutterapp/block/drop_down/drop_down_bloc.dart';

class DropDownWidget extends StatelessWidget {
  @override
  final Key? key;

  const DropDownWidget({this.key}) : super(key: key);

  BlocProvider getBlocProvider() {
    return BlocProvider(
        create: (context) => DropDownBloc()..add(const LoadDropDown()));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [getBlocProvider()],
        child: BlocBuilder<DropDownBloc, DropDownState>(
          builder: (context, state) {
            if (state is DropDownLoaded) {
              return DropdownButtonFormField<int>(
                isExpanded: true,
                value: state.selectedValue < state.dropdownModel.length
                    ? state.selectedValue
                    : 0,
                items: state.dropdownModel.map((item) {
                  return DropdownMenuItem<int>(
                    value: item.id,
                    child: Text(
                      item.value,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList(),
                onChanged: (int? newValue) {
                  context.read<DropDownBloc>().add(UpdateDropDown(
                      dropdownModel: state.dropdownModel,
                      selectedValue: newValue! < state.dropdownModel.length
                          ? newValue
                          : 0));
                },
                decoration: InputDecoration(
                  hintText: "Select an item",
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ));
  }
}

class MaterialDropDown extends StatelessWidget {
  final String? selectedValue;
  final Function(String)? onValueChange;
  const MaterialDropDown(
      {super.key, this.selectedValue = "", this.onValueChange});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) =>
                  MaterialDropDownBloc(onValueChange: onValueChange)
                    ..add(LoadDropDown(selectedValue: selectedValue!)))
        ],
        child: BlocBuilder<MaterialDropDownBloc, DropDownState>(
          builder: (context, state) {
            if (state is DropDownLoaded) {
              return DropdownButtonFormField<int>(
                isExpanded: true,
                value: state.selectedValue > 0 &&
                        state.selectedValue <= (state.dropdownModel.length + 1)
                    ? state.selectedValue
                    : null,
                items: state.dropdownModel.map((item) {
                  return DropdownMenuItem<int>(
                    value: item.id,
                    child: Text(
                      item.value,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList(),
                onChanged: (int? newValue) {
                  context.read<MaterialDropDownBloc>().add(UpdateDropDown(
                      dropdownModel: state.dropdownModel,
                      selectedValue: newValue!));
                },
                decoration: InputDecoration(
                  hintText: "Select an item",
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ));
  }
}

class HeatingDropDown extends StatelessWidget {
  final String? selectedValue;
  final Function(String)? onValueChange;
  const HeatingDropDown(
      {super.key, this.selectedValue = "", this.onValueChange});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) =>
                  HeatingDropDownBloc(onValueChange: onValueChange)
                    ..add(LoadDropDown(selectedValue: selectedValue!)))
        ],
        child: BlocBuilder<HeatingDropDownBloc, DropDownState>(
          builder: (context, state) {
            if (state is DropDownLoaded) {
              return DropdownButtonFormField<int>(
                isExpanded: true,
                value: state.selectedValue > 0 &&
                        state.selectedValue < state.dropdownModel.length
                    ? state.selectedValue
                    : null,
                items: state.dropdownModel.map((item) {
                  return DropdownMenuItem<int>(
                    value: item.id,
                    child: Text(
                      item.value,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList(),
                onChanged: (int? newValue) {
                  context.read<HeatingDropDownBloc>().add(UpdateDropDown(
                      dropdownModel: state.dropdownModel,
                      selectedValue: newValue! < state.dropdownModel.length
                          ? newValue
                          : 0));
                },
                decoration: InputDecoration(
                  hintText: "Select an item",
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ));
  }
}

class CantonDropDown extends StatelessWidget {
  final String? selectedValue;
  final Function(String)? onValueChange;
  const CantonDropDown(
      {super.key, this.selectedValue = "", this.onValueChange});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) =>
                  CantonDropDownBloc(onValueChange: onValueChange)
                    ..add(LoadDropDown(selectedValue: selectedValue!)))
        ],
        child: BlocBuilder<CantonDropDownBloc, DropDownState>(
          builder: (context, state) {
            if (state is DropDownLoaded) {
              return DropdownButtonFormField<int>(
                isExpanded: true,
                value: state.selectedValue > 0 &&
                        state.selectedValue < state.dropdownModel.length
                    ? state.selectedValue
                    : null,
                items: state.dropdownModel.map((item) {
                  return DropdownMenuItem<int>(
                    value: item.id,
                    child: Text(
                      item.value,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList(),
                onChanged: (int? newValue) {
                  context.read<CantonDropDownBloc>().add(UpdateDropDown(
                      dropdownModel: state.dropdownModel,
                      selectedValue: newValue! < state.dropdownModel.length
                          ? newValue
                          : 0));
                },
                decoration: InputDecoration(
                  hintText: "Select an item",
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ));
  }
}

class WeatherDropDown extends StatelessWidget {
  final String? selectedValue;
  final Function(String)? onValueChange;
  const WeatherDropDown(
      {super.key, this.selectedValue = "", this.onValueChange});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) =>
                  WeaterDropDownBloc(onValueChange: onValueChange)
                    ..add(LoadDropDown(selectedValue: selectedValue!)))
        ],
        child: BlocBuilder<WeaterDropDownBloc, DropDownState>(
          builder: (context, state) {
            if (state is DropDownLoaded) {
              return DropdownButtonFormField<int>(
                isExpanded: true,
                value: state.selectedValue > 0 &&
                        state.selectedValue < state.dropdownModel.length
                    ? state.selectedValue
                    : null,
                items: state.dropdownModel.map((item) {
                  return DropdownMenuItem<int>(
                    value: item.id,
                    child: Text(
                      item.value,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList(),
                onChanged: (int? newValue) {
                  context.read<WeaterDropDownBloc>().add(UpdateDropDown(
                      dropdownModel: state.dropdownModel,
                      selectedValue: newValue! < state.dropdownModel.length
                          ? newValue
                          : 0));
                },
                decoration: InputDecoration(
                  hintText: "Select an item",
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ));
  }
}

class InsulatorDropDown extends StatelessWidget {
  final String? selectedValue;
  final Function(String)? onValueChange;
  const InsulatorDropDown(
      {super.key, this.selectedValue = "", this.onValueChange});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) =>
                  InsulatorDropDownBloc(onValueChange: onValueChange)
                    ..add(LoadDropDown(selectedValue: selectedValue!)))
        ],
        child: BlocBuilder<InsulatorDropDownBloc, DropDownState>(
          builder: (context, state) {
            if (state is DropDownLoaded) {
              return DropdownButtonFormField<int>(
                isExpanded: true,
                value: state.selectedValue > 0 ? state.selectedValue : null,
                items: state.dropdownModel.map((item) {
                  return DropdownMenuItem<int>(
                    value: item.id,
                    child: Text(
                      item.value,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList(),
                onChanged: (int? newValue) {
                  context.read<InsulatorDropDownBloc>().add(UpdateDropDown(
                      dropdownModel: state.dropdownModel,
                      selectedValue: newValue! < state.dropdownModel.length
                          ? newValue
                          : 0));
                },
                decoration: InputDecoration(
                  hintText: "Select an item",
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ));
  }
}
