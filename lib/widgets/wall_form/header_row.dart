import 'package:flutter/material.dart';
import 'package:pi_flutterapp/src/resources/app_colors.dart';

class HeaderRow extends StatelessWidget {
  const HeaderRow({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      color: AppColors.headerWallForm,
      child: const Row(
        children: [
          Expanded(
            flex: 5,
            child: Text(
              'Matériau',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            flex: 2,
            child: Text(
              'Largeur [cm]',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            flex: 1,
            child: Text(
              'Porteur',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
