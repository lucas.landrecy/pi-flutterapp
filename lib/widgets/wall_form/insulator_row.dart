import 'package:flutter/material.dart';
import 'package:pi_flutterapp/widgets/wall_form/drop_down_widget.dart';

class InsulatorDropdownRow extends StatelessWidget {
  final String? selectedInsulator;
  final Function(String?) onInsulatorChanged;

  const InsulatorDropdownRow({
    super.key,
    this.selectedInsulator,
    required this.onInsulatorChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Isolant souhaité',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 10),
        Row(
          children: [
            Expanded(
              flex: 10,
              child: InsulatorDropDown(
                selectedValue: selectedInsulator,
                onValueChange: onInsulatorChanged,
              ),
            ),
            const SizedBox(width: 30),

            const Expanded(
              flex: 1,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Radio(
                    value: null,
                    groupValue: null,
                    onChanged: null,
                  ),
                ],
              ),
            ),
            const SizedBox(width: 20),
            const Expanded(
              flex: 1,
              child: SizedBox(width: 20),
            ) // Empty space for invisible add/delete button
          ],
        ),
      ],
    );
  }
}
