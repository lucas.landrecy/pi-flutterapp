import 'package:flutter/material.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';
import 'package:pi_flutterapp/models/material_form_model.dart';
import 'package:pi_flutterapp/widgets/wall_form/drop_down_widget.dart';

class MaterialFormWidget extends StatelessWidget {
  @override
  // ignore: overridden_fields
  final Key? key;
  final MaterialFormModel materialFormModel;
  final int radioValue;
  final void Function(double) onWidthChanged;
  final void Function(int?) onRadioChanged;
  final StatelessWidget btnAddDell;
  final Function(String)? onMaterialChanged;

  const MaterialFormWidget(
      {this.key,
      required this.materialFormModel,
      required this.radioValue,
      required this.onWidthChanged,
      required this.onRadioChanged,
      required this.btnAddDell,
      this.onMaterialChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
                flex: 6,
                child: MaterialDropDown(
                  selectedValue: materialFormModel.materialSelected,
                  onValueChange: onMaterialChanged,
                )),
            const SizedBox(width: 20),
            Expanded(
              flex: 3,
              child: SpinBox(
                min: 0.0,
                max: 120.0,
                decimals: 2,
                value: materialFormModel.width,
                onChanged: onWidthChanged,
              ),
            ),
            const SizedBox(width: 20),
            Expanded(
              flex: 1,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Radio(
                    value: materialFormModel.index,
                    groupValue: radioValue,
                    onChanged: onRadioChanged,
                  ),
                ],
              ),
            ),
            const SizedBox(width: 10),
            Expanded(flex: 1, child: btnAddDell)
          ],
        ),
      ],
    );
  }
}
