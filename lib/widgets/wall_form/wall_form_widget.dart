import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pi_flutterapp/block/material_line/material_form_bloc.dart';
import 'package:pi_flutterapp/models/material_form_model.dart';
import 'package:pi_flutterapp/widgets/wall_form/drop_down_widget.dart';
import 'package:pi_flutterapp/widgets/wall_form/header_row.dart';
import 'package:pi_flutterapp/widgets/wall_form/insulator_row.dart';
import 'package:pi_flutterapp/widgets/wall_form/material_form_widget.dart';

class WallFormWidget extends StatelessWidget {
  @override
  // ignore: overridden_fields
  final Key? key;
  final void Function(
      List<MaterialFormModel>?, String?, String?, String?, String?) onUpdated;
  // List<MaterialFormModel>? materialFormModel, String? canton, String? heating, String? insulator, String? weather

  final void Function()? onSubmitted;
  const WallFormWidget(
      {this.key, required this.onUpdated, required this.onSubmitted})
      : super(key: key);

  StatelessWidget btnAddDel(
      int index, MaterialFormLoaded state, BuildContext context) {
    final int length = state.materialFormModel.length;
    if ((index == 0)) {
      return Container();
    } else if (index == length - 1) {
      return IconButton(
        icon: const Icon(Icons.add),
        onPressed: () {
          onUpdated(state.materialFormModel, null, null, null, null);
          context.read<MaterialFormBloc>().add(AddMaterial(
              materialFormModel: MaterialFormModel(
                  materialSelected: "", width: 1, isPorteur: false)));
        },
      );
    } else {
      return IconButton(
        icon: const Icon(Icons.delete),
        onPressed: () {
          onUpdated(state.materialFormModel, null, null, null, null);
          context.read<MaterialFormBloc>().add(DeleteMaterial(index: index));
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height - 120),
      child: MultiBlocProvider(
          providers: [
            BlocProvider(
                create: (context) =>
                    MaterialFormBloc()..add(LoadMaterialForm())),
          ],
          child: BlocBuilder<MaterialFormBloc, MaterialFormState>(
              builder: (context, state) {
            if (state is MaterialFormInitial) {
              return const CircularProgressIndicator();
            } else if (state is MaterialFormLoaded) {
              int radioValue = state.materialFormModel
                  .indexWhere((element) => element.isPorteur);
              if (radioValue == -1) {
                radioValue = 0;
              }
              return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Heating, Canton and Weather dropdown
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Ligne des entetes pour les dropdown Canton, Chauffage et Zone Météo
                            const Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Chauffage',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )),
                                SizedBox(width: 20),
                                Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Canton Subvention',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )),
                                SizedBox(width: 20),
                                Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Région / Climat',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )),
                              ],
                            ),
                            const SizedBox(height: 10),
                            // Ligne des dropdown Canton, Chauffage et Zone Météo
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: HeatingDropDown(
                                    selectedValue: state.heatingSelected,
                                    onValueChange: (value) {
                                      context.read<MaterialFormBloc>().add(
                                          UpdateSelected(
                                              heatingSelected: value,
                                              cantonSelected:
                                                  state.cantonSelected,
                                              weatherSelected:
                                                  state.weatherSelected,
                                              insolatorSelected:
                                                  state.insulatorSelected));
                                      onUpdated(null, null, value, null, null);
                                    },
                                  ),
                                ),
                                const SizedBox(width: 20),
                                Expanded(
                                  flex: 1,
                                  child: CantonDropDown(
                                    selectedValue: state.heatingSelected,
                                    onValueChange: (value) {
                                      context.read<MaterialFormBloc>().add(
                                          UpdateSelected(
                                              cantonSelected: value,
                                              heatingSelected:
                                                  state.heatingSelected,
                                              weatherSelected:
                                                  state.weatherSelected,
                                              insolatorSelected:
                                                  state.insulatorSelected));
                                      onUpdated(null, value, null, null, null);
                                    },
                                  ),
                                ),
                                const SizedBox(width: 20),
                                Expanded(
                                  flex: 1,
                                  child: WeatherDropDown(
                                    selectedValue: state.heatingSelected,
                                    onValueChange: (value) {
                                      context.read<MaterialFormBloc>().add(
                                          UpdateSelected(
                                              weatherSelected: value,
                                              cantonSelected:
                                                  state.cantonSelected,
                                              heatingSelected:
                                                  state.heatingSelected,
                                              insolatorSelected:
                                                  state.insulatorSelected));
                                      onUpdated(null, null, null, null, value);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ]),
                    ),
                    const SizedBox(height: 10),

                    // Header Row
                    const HeaderRow(),
                    const SizedBox(height: 10),

                    // Insulator Dropdown
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              InsulatorDropdownRow(
                                selectedInsulator: state.insulatorSelected,
                                onInsulatorChanged: (value) {
                                  context.read<MaterialFormBloc>().add(
                                      UpdateSelected(
                                          insolatorSelected: value!,
                                          cantonSelected: state.cantonSelected,
                                          heatingSelected:
                                              state.heatingSelected,
                                          weatherSelected:
                                              state.weatherSelected));
                                  onUpdated(null, null, null, value, null);
                                },
                              ),
                              const Text(
                                'Composition du mur',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(height: 10),
                              Expanded(
                                  child: ListView.separated(
                                separatorBuilder: (context, index) =>
                                    const SizedBox(height: 20),
                                itemCount: state.materialFormModel.length,
                                itemBuilder: (context, index) {
                                  final materialFormModel =
                                      state.materialFormModel[index];
                                  return MaterialFormWidget(
                                    key: ValueKey(materialFormModel.index),
                                    materialFormModel: materialFormModel,
                                    radioValue: state
                                        .materialFormModel[radioValue].index,
                                    onWidthChanged: (double value) {
                                      onUpdated(state.materialFormModel, null,
                                          null, null, null);
                                      context.read<MaterialFormBloc>().add(
                                          UpdateWidthMaterial(
                                              index: index, value: value));
                                    },
                                    onRadioChanged: (int? value) {
                                      onUpdated(state.materialFormModel, null,
                                          null, null, null);
                                      context.read<MaterialFormBloc>().add(
                                          UpdateRadioMaterial(
                                              materialFormModel:
                                                  materialFormModel,
                                              value: value!));
                                    },
                                    btnAddDell:
                                        btnAddDel(index, state, context),
                                    onMaterialChanged: (value) {
                                      onUpdated(state.materialFormModel, null,
                                          null, null, null);
                                      context.read<MaterialFormBloc>().add(
                                          UpdateMaterial(
                                              index: index,
                                              materialSelected: value));
                                    },
                                  );
                                },
                              )),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                ElevatedButton(
                                  onPressed: () {
                                    onSubmitted!();
                                  },
                                  style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                                    textStyle: const TextStyle(fontSize: 18),
                                  ),
                                  child: const Text('Calculer'),
                                ),]),
                            ]),
                      ),
                    )
                  ]);
            } else {
              return const Text("Sommething went wrong!");
            }
          })),
    );
  }
}
