import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';
import 'package:pi_flutterapp/block/grant_form/grant_form_bloc.dart';
import 'package:pi_flutterapp/src/db/data_class.dart';
import 'package:pi_flutterapp/widgets/wall_form/drop_down_widget.dart';

class GrantFormWidget extends StatefulWidget {
  const GrantFormWidget({super.key});

  @override
  State<StatefulWidget> createState() => _GrantForm();
}

class _GrantForm extends State<GrantFormWidget> {
  bool isLower = true;
  double U = 0.0;
  double amountChfBy = 0.0;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) =>
                  GrantFormBloc()..add(const LoadGrantForm(canton: 'Genève'))),
        ],
        child: BlocBuilder<GrantFormBloc, GrantFormState>(
            builder: (context, state) {
          if (state is GrantFormInitial) {
            return const CircularProgressIndicator();
          } else if (state is GrantFormLoaded) {
            return Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  const Text("Sélectionner un Canton"),
                  CantonDropDown(
                    selectedValue: state.canton.name,
                    onValueChange: (String canton) {
                      context
                          .read<GrantFormBloc>()
                          .add(UpdateCanton(canton: Canton(id: -1, name: canton)));
                    },
                  ),
                  const SizedBox(height: 20),
                  header(),
                  const SizedBox(height: 20),
                  ConstrainedBox(
                      constraints: const BoxConstraints(maxHeight: 400),
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.grants.length,
                          itemBuilder: (context, index) {
                            return row(context, state, index);
                          })),
                  rowAdd(state, () {
                    context.read<GrantFormBloc>().add(AddGrant(
                            grant: Grant(
                          id: -1,
                          isLower: isLower,
                          nameCanton: state.canton.name,
                          U: U,
                          amountChfBy: amountChfBy,
                        )));
                    setState(() {
                      isLower = true;
                      U = 0.0;
                      amountChfBy = 0.0;
                    });
                  }),
                ],
              ),
            );
          } else {
            return const Text("Error");
          }
        }));
  }

  Widget header() {
    return const Row(
      children: [
        Expanded(
          flex: 1,
          child: Text("Entrée"),
        ),
        SizedBox(width: 20),
        Expanded(
          flex: 1,
          child: Text(""),
        ),
        SizedBox(width: 20),
        Expanded(
          flex: 3,
          child: Text("U"),
        ),
        SizedBox(width: 20),
        Expanded(
          flex: 3,
          child: Text("CHF/m^2"),
        ),
        SizedBox(width: 20),
        Expanded(
          flex: 1,
          child: Text("Ajouter"),
        ),
      ],
    );
  }

  Widget row(BuildContext context, GrantFormLoaded state, int index) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Text(index.toString()),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 1,
          child: IconButton(
            icon: Icon((state.grants[index].isLower)
                ? Icons.chevron_left
                : Icons.chevron_right),
                onPressed: () {
                  
                
            // onPressed: () {
            //   final current = state.grants[index];
            //   context.read<GrantFormBloc>().add(UpdateGrant(
            //       index: index,
            //       grant: Grant(
            //         id: current.id,
            //         isLower: !current.isLower,
            //         nameCanton: current.nameCanton,
            //         U: current.U,
            //         amountChfBy: current.amountChfBy,
            //       )));
            },
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 3,
          child: SpinBox(
            min: 0.0,
            max: 1.0,
            decimals: 2,
            step: 0.01,
            value: state.grants[index].U,
            onChanged: (double value) {
              final current = state.grants[index];
              context.read<GrantFormBloc>().add(UpdateGrant(
                  index: index,
                  grant: Grant(
                    id: current.id,
                    isLower: current.isLower,
                    nameCanton: current.nameCanton,
                    U: value,
                    amountChfBy: current.amountChfBy,
                  )));
            },
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 3,
          child: SpinBox(
            min: 0.0,
            max: 100.0,
            decimals: 2,
            value: state.grants[index].amountChfBy,
            onChanged: (double value) {
              final current = state.grants[index];
              context.read<GrantFormBloc>().add(UpdateGrant(
                  index: index,
                  grant: Grant(
                    id: current.id,
                    isLower: current.isLower,
                    nameCanton: current.nameCanton,
                    U: current.U,
                    amountChfBy: value,
                  )));
            },
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 1,
          child: IconButton(
            icon: const Icon(Icons.delete),
            onPressed: () {
              context.read<GrantFormBloc>().add(DeleteGrant(index: index));
            },
          ),
        ),
      ],
    );
  }

  Widget rowAdd(GrantFormLoaded state, void Function() onPresse) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Text(state.grants.length.toString()),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 1,
          child: IconButton(
            icon: Icon((isLower) ? Icons.chevron_left : Icons.chevron_right),
            onPressed: () {
              // setState(() {
              //   isLower = !isLower;
              // });
            },
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 3,
          child: SpinBox(
            min: 0.0,
            max: 1.0,
            decimals: 2,
            step: 0.01,
            value: U,
            onChanged: (double value) {
              setState(() {
                U = value;
              });
            },
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 3,
          child: SpinBox(
            min: 0.0,
            max: 100.0,
            decimals: 2,
            value: amountChfBy,
            onChanged: (double value) {
              setState(() {
                amountChfBy = value;
              });
            },
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          flex: 1,
          child: IconButton(icon: const Icon(Icons.add), onPressed: onPresse),
        ),
      ],
    );
  }
}
