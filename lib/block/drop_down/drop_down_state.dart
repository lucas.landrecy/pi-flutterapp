part of 'drop_down_bloc.dart';

sealed class DropDownState extends Equatable {
  const DropDownState();

  @override
  List<Object> get props => [];
}

final class DropDownInitial extends DropDownState {}

final class DropDownLoading extends DropDownState {}

final class DropDownLoaded extends DropDownState {
  final List<DropdownModel> dropdownModel;
  final int selectedValue;

  const DropDownLoaded(
      {required this.dropdownModel, required this.selectedValue});

  @override
  List<Object> get props => [dropdownModel, selectedValue];
}

final class DropDownEmpty extends DropDownState {}
