part of 'drop_down_bloc.dart';

sealed class DropDownEvent extends Equatable {
  const DropDownEvent();

  @override
  List<Object> get props => [];
}

class LoadDropDown extends DropDownEvent {
  final String? selectedValue;

  const LoadDropDown({this.selectedValue = ""});

  @override
  List<Object> get props => [selectedValue!];
}

class UpdateDropDown extends DropDownEvent {
  final List<DropdownModel> dropdownModel;
  final int selectedValue;

  const UpdateDropDown(
      {required this.dropdownModel, required this.selectedValue});

  @override
  List<Object> get props => [dropdownModel, selectedValue];
}
