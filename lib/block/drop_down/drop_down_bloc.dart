// ignore_for_file: unused_local_variable

import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pi_flutterapp/models/dropdown_model.dart';
import 'package:pi_flutterapp/src/db/data_base.dart';

part 'drop_down_event.dart';
part 'drop_down_state.dart';

class DropDownBloc extends Bloc<DropDownEvent, DropDownState> {
  final FutureOr<void> Function(LoadDropDown, Emitter<DropDownState>)
      onLoadDropDown;
  final FutureOr<void> Function(UpdateDropDown, Emitter<DropDownState>)
      onUpdateDropDown;

  static Future<void> _defaultLoadDropDown(
      LoadDropDown event, Emitter<DropDownState> emit) async {
    emit(const DropDownLoaded(
        dropdownModel: [DropdownModel(id: 0, value: "")], selectedValue: 0));
  }

  static Future<void> _defaultUpdateDropDown(
      UpdateDropDown event, Emitter<DropDownState> emit) async {
    emit(DropDownLoaded(
        dropdownModel: event.dropdownModel,
        selectedValue: event.selectedValue));
  }

  DropDownBloc(
      {this.onLoadDropDown = _defaultLoadDropDown,
      this.onUpdateDropDown = _defaultUpdateDropDown})
      : super(DropDownInitial()) {
    on<LoadDropDown>(onLoadDropDown);

    on<UpdateDropDown>(onUpdateDropDown);
  }
}

class MaterialDropDownBloc extends DropDownBloc {
  final Function(String)? onValueChange;
  MaterialDropDownBloc({this.onValueChange})
      : super(onLoadDropDown: (event, emit) async {
          DataBase db = DataBase.getInstance();
          final dropdownModel = await db.getMaterialsByType([
            "Béton",
            "Pierres de taille",
            "Composite ciment-verre",
            "Mortiers et enduits",
            "Bois et produits en bois",
            "Enduits et revêtements",
            "Autres matériaux massifs",
          ]);
          int i = 1;
          List<DropdownModel> materialList = dropdownModel.keys.map((e) {
            return DropdownModel(id: i++, value: e);
          }).toList();

          int selectedValue = materialList
              .indexWhere((element) => element.value == event.selectedValue);

          if (selectedValue != -1) {
            selectedValue = materialList[selectedValue].id;
          }

          emit(DropDownLoaded(
              dropdownModel: materialList, selectedValue: selectedValue));
        }, onUpdateDropDown: (event, emit) async {
          emit(DropDownLoaded(
              dropdownModel: event.dropdownModel,
              selectedValue: event.selectedValue));
          onValueChange!(event.dropdownModel[(event.selectedValue - 1)].value);
        });
}

class HeatingDropDownBloc extends DropDownBloc {
  final Function(String)? onValueChange;
  HeatingDropDownBloc({this.onValueChange})
      : super(onLoadDropDown: (event, emit) async {
          DataBase db = DataBase.getInstance();
          final dropdownModel = await db.getHeatings();
          int i = 1;
          List<DropdownModel> heatingList = dropdownModel.keys
              .map((e) => DropdownModel(id: i++, value: e))
              .toList();

          int selectedValue = heatingList
              .indexWhere((element) => element.value == event.selectedValue);

          if (selectedValue != -1) {
            selectedValue = heatingList[selectedValue].id;
          }

          emit(DropDownLoaded(
              dropdownModel: heatingList, selectedValue: selectedValue));
        }, onUpdateDropDown: (event, emit) async {
          emit(DropDownLoaded(
              dropdownModel: event.dropdownModel,
              selectedValue: event.selectedValue));
              print(  event.dropdownModel[event.selectedValue-1].value);
          onValueChange!(event.dropdownModel[event.selectedValue-1].value);
        });
}

class CantonDropDownBloc extends DropDownBloc {
  final Function(String)? onValueChange;
  CantonDropDownBloc({this.onValueChange})
      : super(onLoadDropDown: (event, emit) async {
          DataBase db = DataBase.getInstance();
          final dropdownModel = await db.getCantons();
          int i = 1;
          List<DropdownModel> heatingList = dropdownModel
              .map((e) => DropdownModel(id: i++, value: e.name))
              .toList();

          int selectedValue = heatingList
              .indexWhere((element) => element.value == event.selectedValue);

          if (selectedValue != -1) {
            selectedValue = heatingList[selectedValue].id;
          }

          emit(DropDownLoaded(
              dropdownModel: heatingList, selectedValue: selectedValue));
        }, onUpdateDropDown: (event, emit) async {
          emit(DropDownLoaded(
              dropdownModel: event.dropdownModel,
              selectedValue: event.selectedValue));
          onValueChange!(event.dropdownModel[(event.selectedValue-1)].value);
        });
}

class WeaterDropDownBloc extends DropDownBloc {
  final Function(String)? onValueChange;
  WeaterDropDownBloc({this.onValueChange})
      : super(onLoadDropDown: (event, emit) async {
          final dropdownModel = await DataBase.getCitytemp();
          int i = 1;
          List<DropdownModel> heatingList = dropdownModel
              .map((e) => DropdownModel(id: i++, value: e.name))
              .toList();

          int selectedValue = heatingList
              .indexWhere((element) => element.value == event.selectedValue);

          if (selectedValue != -1) {
            selectedValue = heatingList[selectedValue].id;
          }

          emit(DropDownLoaded(
              dropdownModel: heatingList, selectedValue: selectedValue));
        }, onUpdateDropDown: (event, emit) async {
          emit(DropDownLoaded(
              dropdownModel: event.dropdownModel,
              selectedValue: event.selectedValue));
              print(event.dropdownModel[event.selectedValue-1].value);
          onValueChange!(event.dropdownModel[event.selectedValue-1].value);
        });
}

class InsulatorDropDownBloc extends DropDownBloc {
  final Function(String)? onValueChange;
  InsulatorDropDownBloc({this.onValueChange})
      : super(onLoadDropDown: (event, emit) async {
          DataBase db = DataBase.getInstance();
          final dropdownModel = await db.getIsolants();
          int i = 1;
          List<DropdownModel> heatingList = dropdownModel.keys
              .map((e) => DropdownModel(id: i++, value: e))
              .toList();

          int selectedValue = heatingList
              .indexWhere((element) => element.value == event.selectedValue);

          if (selectedValue != -1) {
            selectedValue = heatingList[selectedValue].id;
          }

          emit(DropDownLoaded(
              dropdownModel: heatingList, selectedValue: selectedValue));
        }, onUpdateDropDown: (event, emit) async {
          emit(DropDownLoaded(
              dropdownModel: event.dropdownModel,
              selectedValue: event.selectedValue));
              print(event.dropdownModel[event.selectedValue-1].value);
          onValueChange!(event.dropdownModel[event.selectedValue-1].value);
        });
}
