part of 'grant_form_bloc.dart';

sealed class GrantFormState extends Equatable {
  const GrantFormState();

  @override
  List<Object> get props => [];
}

final class GrantFormInitial extends GrantFormState {}

final class GrantFormLoading extends GrantFormState {}

final class GrantFormLoaded extends GrantFormState {
  final Canton canton;
  final List<Grant> grants;

  const GrantFormLoaded({required this.canton, required this.grants});

  @override
  List<Object> get props => [canton, grants];
}
