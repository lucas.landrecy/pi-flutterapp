part of 'grant_form_bloc.dart';

sealed class GrantFormEvent extends Equatable {
  const GrantFormEvent();

  @override
  List<Object> get props => [];
}

class LoadGrantForm extends GrantFormEvent {
  final String canton;

  const LoadGrantForm({required this.canton});

  @override
  List<Object> get props => [Canton];
}

class UpdateCanton extends GrantFormEvent {
  final Canton canton;

  const UpdateCanton({required this.canton});

  @override
  List<Object> get props => [canton];
}

class UpdateGrant extends GrantFormEvent {
  final Grant grant;
  final int index;

  const UpdateGrant({required this.grant, required this.index});

  @override
  List<Object> get props => [grant, index];
}

class DeleteGrant extends GrantFormEvent {
  final int index;

  const DeleteGrant({required this.index});

  @override
  List<Object> get props => [index];
}

class AddGrant extends GrantFormEvent {
  final Grant grant;

  const AddGrant({required this.grant});

  @override
  List<Object> get props => [grant];
}
