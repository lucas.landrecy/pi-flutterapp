import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:pi_flutterapp/src/db/data_base.dart';
import 'package:pi_flutterapp/src/db/data_class.dart';

part 'grant_form_event.dart';
part 'grant_form_state.dart';

class GrantFormBloc extends Bloc<GrantFormEvent, GrantFormState> {
  GrantFormBloc() : super(GrantFormInitial()) {
    on<LoadGrantForm>((event, emit) async {
      DataBase db = DataBase.getInstance();
      final grantList = await db.getGrants(event.canton);
      final canton = await db.getCanton(event.canton);

      emit(GrantFormLoaded(canton: canton, grants: grantList));
    });
    on<UpdateCanton>((event, emit) async {
      print(event.canton.name);
      DataBase db = DataBase.getInstance();
      final grantList = await db.getGrants(event.canton.name);

      emit(GrantFormLoaded(canton: event.canton, grants: grantList));
    });

    on<UpdateGrant>((event, emit) async {
      final currentState = state;
      if (currentState is GrantFormLoaded) {
        final List<Grant> newGrants = List.from(currentState.grants);
        newGrants[event.index] = event.grant;

        print('Update grant: ${event.grant.nameCanton}, ${event.grant.amountChfBy}, ${event.grant.U}');

        DataBase db = DataBase.getInstance();
        await db.updateGrant(event.grant);

        emit(GrantFormLoaded(canton: currentState.canton, grants: newGrants));
      }
    });

    on<DeleteGrant>((event, emit) async {
      final currentState = state;
      if (currentState is GrantFormLoaded) {
        final List<Grant> newGrants = List.from(currentState.grants);
        newGrants.removeAt(event.index);

        DataBase db = DataBase.getInstance();
        await db.deleteGrant(currentState.grants[event.index]);

        emit(GrantFormLoaded(canton: currentState.canton, grants: newGrants));
      }
    });

    on<AddGrant>((event, emit) {
      final currentState = state;
      if (currentState is GrantFormLoaded) {
        final List<Grant> newGrants = List.from(currentState.grants);
        newGrants.add(event.grant);
        print(event.grant.nameCanton);

        DataBase db = DataBase.getInstance();
        db.insertGrant(event.grant);

        emit(GrantFormLoaded(canton: currentState.canton, grants: newGrants));
      }
    });
  }
}
