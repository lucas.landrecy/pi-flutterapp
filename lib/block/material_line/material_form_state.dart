part of 'material_form_bloc.dart';

sealed class MaterialFormState extends Equatable {
  const MaterialFormState();

  @override
  List<Object> get props => [];
}

final class MaterialFormInitial extends MaterialFormState {}

final class MaterialFormLoading extends MaterialFormState {}

final class MaterialFormLoaded extends MaterialFormState {
  final List<MaterialFormModel> materialFormModel;
  final String cantonSelected;
  final String heatingSelected;
  final String insulatorSelected;
  final String weatherSelected;

  const MaterialFormLoaded(
      {required this.materialFormModel,
      required this.cantonSelected,
      required this.heatingSelected,
      required this.insulatorSelected,
      required this.weatherSelected});

  @override
  List<Object> get props => [
        materialFormModel,
        cantonSelected,
        heatingSelected,
        insulatorSelected,
        weatherSelected
      ];
}
