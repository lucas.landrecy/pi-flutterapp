part of 'material_form_bloc.dart';

sealed class MaterialFormEvent extends Equatable {
  const MaterialFormEvent();

  @override
  List<Object> get props => [];
}

class LoadMaterialForm extends MaterialFormEvent {}

class AddMaterial extends MaterialFormEvent {
  final MaterialFormModel materialFormModel;

  const AddMaterial({required this.materialFormModel});

  @override
  List<Object> get props => [materialFormModel];
}

class UpdateMaterial extends MaterialFormEvent {
  final int index;
  final String materialSelected;

  const UpdateMaterial({required this.index, required this.materialSelected});

  @override
  List<Object> get props => [index, materialSelected];
}

class UpdateRadioMaterial extends MaterialFormEvent {
  final MaterialFormModel materialFormModel;
  final int value;

  const UpdateRadioMaterial(
      {required this.materialFormModel, required this.value});

  @override
  List<Object> get props => [materialFormModel, value];
}

class UpdateWidthMaterial extends MaterialFormEvent {
  final int index;
  final double value;

  const UpdateWidthMaterial({required this.index, required this.value});

  @override
  List<Object> get props => [index, value];
}

class UpdateSelected extends MaterialFormEvent {
  final String insolatorSelected;
  final String heatingSelected;
  final String cantonSelected;
  final String weatherSelected;

  const UpdateSelected({
    required this.insolatorSelected,
    required this.heatingSelected,
    required this.cantonSelected,
    required this.weatherSelected,
  });

  @override
  List<Object> get props =>
      [insolatorSelected, heatingSelected, cantonSelected, weatherSelected];
}

class DeleteMaterial extends MaterialFormEvent {
  final int index;

  const DeleteMaterial({required this.index});

  @override
  List<Object> get props => [index];
}
