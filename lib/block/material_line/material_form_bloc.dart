import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pi_flutterapp/models/material_form_model.dart';

part 'material_form_event.dart';
part 'material_form_state.dart';

class MaterialFormBloc extends Bloc<MaterialFormEvent, MaterialFormState> {
  MaterialFormBloc() : super(MaterialFormInitial()) {
    on<LoadMaterialForm>((event, emit) {
      emit(MaterialFormLoaded(
          materialFormModel: [
            MaterialFormModel(materialSelected: "", width: 1, isPorteur: false),
            MaterialFormModel(materialSelected: "", width: 1, isPorteur: false),
          ],
          cantonSelected: "",
          heatingSelected: "",
          insulatorSelected: "",
          weatherSelected: ""));
    });

    on<AddMaterial>((event, emit) {
      final currentState = state;
      if (currentState is MaterialFormLoaded) {
        final List<MaterialFormModel> updatedMaterialFormModel =
            List.from(currentState.materialFormModel)
              ..add(event.materialFormModel);
        emit(MaterialFormLoaded(
            materialFormModel: updatedMaterialFormModel,
            cantonSelected: currentState.cantonSelected,
            heatingSelected: currentState.heatingSelected,
            insulatorSelected: currentState.insulatorSelected,
            weatherSelected: currentState.weatherSelected));
      }
    });

    on<UpdateMaterial>((event, emit) {
      final currentState = state;
      if (currentState is MaterialFormLoaded) {
        final element = currentState.materialFormModel[event.index];

        final List<MaterialFormModel> updatedMaterialFormModel =
            List.from(currentState.materialFormModel);

        updatedMaterialFormModel[event.index] = MaterialFormModel(
            materialSelected: event.materialSelected,
            width: element.width,
            isPorteur: element.isPorteur);

        emit(MaterialFormLoaded(
            materialFormModel: updatedMaterialFormModel,
            cantonSelected: currentState.cantonSelected,
            heatingSelected: currentState.heatingSelected,
            insulatorSelected: currentState.insulatorSelected,
            weatherSelected: currentState.weatherSelected));
      }
    });

    on<UpdateRadioMaterial>((event, emit) {
      final currentState = state;
      if (currentState is MaterialFormLoaded) {
        final List<MaterialFormModel> updatedMaterialFormModel = currentState
            .materialFormModel
            .map((e) => MaterialFormModel(
                materialSelected: e.materialSelected,
                width: e.width,
                isPorteur: e.index == event.value))
            .toList();

        emit(MaterialFormLoaded(
            materialFormModel: updatedMaterialFormModel,
            cantonSelected: currentState.cantonSelected,
            heatingSelected: currentState.heatingSelected,
            insulatorSelected: currentState.insulatorSelected,
            weatherSelected: currentState.weatherSelected));
      }
    });

    on<UpdateWidthMaterial>((event, emit) {
      final currentState = state;
      if (currentState is MaterialFormLoaded) {
        final List<MaterialFormModel> updatedMaterialFormModel =
            List.from(currentState.materialFormModel);

        updatedMaterialFormModel[event.index] = MaterialFormModel(
            materialSelected:
                currentState.materialFormModel[event.index].materialSelected,
            width: event.value,
            isPorteur: currentState.materialFormModel[event.index].isPorteur);

        emit(MaterialFormLoaded(
            materialFormModel: updatedMaterialFormModel,
            cantonSelected: currentState.cantonSelected,
            heatingSelected: currentState.heatingSelected,
            insulatorSelected: currentState.insulatorSelected,
            weatherSelected: currentState.weatherSelected));
      }
    });

    on<UpdateSelected>((event, emit) {
      final currentState = state;
      if (currentState is MaterialFormLoaded) {
        emit(MaterialFormLoaded(
            materialFormModel: currentState.materialFormModel,
            cantonSelected: event.cantonSelected,
            heatingSelected: event.heatingSelected,
            insulatorSelected: event.insolatorSelected,
            weatherSelected: event.weatherSelected));
      }
    });

    on<DeleteMaterial>((event, emit) {
      final currentState = state;
      if (currentState is MaterialFormLoaded) {
        final List<MaterialFormModel> updatedMaterialFormModel =
            List.from(currentState.materialFormModel)..removeAt(event.index);
        emit(MaterialFormLoaded(
            materialFormModel: updatedMaterialFormModel,
            cantonSelected: currentState.cantonSelected,
            heatingSelected: currentState.heatingSelected,
            insulatorSelected: currentState.insulatorSelected,
            weatherSelected: currentState.weatherSelected));
      }
    });
  }
}
