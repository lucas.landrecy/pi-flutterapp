# Liste des subventions par m2

* source : https://www.francsenergie.ch/fr

| Canton      | Montant (CHF) |
|-------------|---------------|
| Genève      |    40 CHF     |
| Fribourg    |    60 CHF     |
| Vaud        |U<=0,20 50 CHF |
| Vaud        |U<=0,15 70 CHF |
