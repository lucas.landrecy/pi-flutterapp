# PI Enerbat
Lucas Landrecy, Johnny Marinho Da Mota, Benjamin Mouchet

Cette application permet d'optimiser le calcul de l'épaisseur d'un isolant sur un batiment et renseigne les données suivantes: **GWP**, **NRE** et **CHF**.

_La base de données est basée sur le CSV OBDEC distribué en classe_ 


## Comment utiliser l'app
1. Lors du premier lancement de l'application, celle-ci va populer sa base de données en fonction des fichiers CSV dans le dossier `src/csv/`
2. L'application se lance sur la page d'accueil
3. L'utilisateur a plusieurs champs à remplir (sous forme de listes déroulantes) avant de pouvoir afficher le graphe. La première ligne permet de rentrer:
    - Le type de chauffage
    - Le canton afin de déterminer la subvention
    - La région (climat)
4. Une fois la première ligne remplie, il peut composer son mur avec:
    - Le premier élément, l'isolant
    - Plusieurs matériaux avec leur largeur respective. De base un mur est composé de 2 matériaux. Pour en ajouter il suffit de presser le bouton "+" et pour en supprimer un la corbeille. **Toutes les valeurs sont en cm**.
    - Un élément doit être désigné porteur grâce au bouton radio.
5. Le graphe se calcule lors de l'appui du bouton _Calculer_ une fois tous les champs remplis


## Onglet subventions
De base l'application n'a pas encore les valeurs des subventions par canton en fonction de la valeur _U_. Il est possible d'y ajouter les valeurs grâce au formulaire de gauche. 
| Exemple de subvention pour le canton de Genève| ![Exemple de formulaire de subventions](./assets/image-1.png)|
|----------|----------|


Les boutons de droite permettent de charger de nouveaux CSV pour les types de chauffage ou les matériaux. 
- Le fichier pour le chauffage doit contenir les colones suivantes: 

| Nom | GWP(Kg-eg CO2/ kWh) | NRE | Prix (cgf/KwH) |
|----------|----------|----------|----------|
- Le fichier pour les matériaux doit contenir les colones suivantes: 

| Materiau | Masse volumique/ surface | WGP Construction | WGP debarras | NRE Construction | NRE debarras | U | Prix |
|----------|----------|----------|----------|----------|----------|----------|----------|